import {TIHRModel} from './TIHRModel';

export class InquiryModel extends TIHRModel {
    id: number | null;
    inquiry_id: string | null;
    name: string;
    email: string;
    phone: string;
    hotel_id: number | null;
    assign_to_id: number | null;
    status_id: number | null;
    sub_status_id: number | null;
    type_id: number | null;
    source_id: number | null;
    check_in_date: any;
    check_out_date: any;
    inquiry_date: any;
    rooms: number | null;
    adults: number | null;
    childs: number | null;
    offer_rate: number | null;
    company_id: number | null;
    travel_agent_id: number | null;
    remark: string;
    note: string;
    booking_id: string | null;
	voucher_no: string | null;
    detailViewType: number | null;
    room_nights: number | null;
    group_name: string;
	invoice_url: string;
	referral: string;

    clear() {
        this.id = null;
        this.inquiry_id = '';
        this.name = '';
        this.email = '';
        this.phone = '';
        this.hotel_id = null;
        this.assign_to_id = null;
        this.status_id = null;
        this.sub_status_id = null;
        this.type_id = null;
        this.source_id = null;
        this.check_in_date = null;
        this.check_out_date = null;
        this.inquiry_date = null;
        this.rooms = 0;
        this.adults = 0;
        this.childs = 0;
        this.offer_rate = null;
        this.company_id = null;
        this.travel_agent_id = null;
        this.booking_id = null;
        this.voucher_no = null;
        this.remark = '';
        this.note = '';
        this.detailViewType = 0;
        this.room_nights = 0;
        this.group_name = '';
        this.invoice_url = '';
        this.referral = '';
    }
}
