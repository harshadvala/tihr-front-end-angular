import {finalize, tap} from 'rxjs/operators';
import {SettingsService} from '../../../services/settings.service';
import {BaseDataSource} from '../../../content/pages/components/apps/e-commerce/_core/models/data-sources/_base.datasource';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';

export class SettingDataSource extends BaseDataSource {

    constructor(private settingsService: SettingsService) {
        super();
    }

    loadDynamicDashboards(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getDynamicDashboards(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadIndustries(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getIndustries(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadCategories(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getCategories(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadExpenseList(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getExpenseList(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadSources(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getSources(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadInquiryTypes(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getInquiryTypes(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
	}

	loadInquiryStatuses(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getInquiryStatuses(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadInquiryPaymentStatuses(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getInquiryPaymentStatuses(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadRoomTypes(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getRoomTypes(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }


    loadBedTypes(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getBedTypes(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadAmenityTypes(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getAmenityTypes(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadAmenities(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getAmenities(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadMealPlans(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getMealPlans(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadHolidays(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getHolidays(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadDiscounts(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getDiscounts(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadTaxes(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getTaxes(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadExtras(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getExtras(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }


    loadHotels(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.settingsService.getHotels(queryParams).pipe(
            tap(res => {
                const result: any = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadHotelRooms(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.settingsService.getHotelRooms(queryParams).pipe(
            tap(res => {
                const result: any = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadRateTypes(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getRateTypes(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadRatePlans(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getRatePlans(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadPaymentModes(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getPaymentModes(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadOrganizations(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.settingsService.getOrganizations(queryParams).pipe(
            tap(res => {
                const result: any = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadEmailTemplates(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.settingsService.getEmailTemplates(queryParams).pipe(
            tap(res => {
                const result: any = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadEmailLog(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.settingsService.getEmailLogs(queryParams).pipe(
            tap(res => {
                const result: any = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadAppSettings(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getApplicationSettings(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadChannelManagerLogs(queryParams: any) {
        this.loadingSubject.next(true);
        this.settingsService.getCMLogs(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }
}
