import {finalize, tap} from 'rxjs/operators';
import {BaseDataSource} from '../../../content/pages/components/apps/e-commerce/_core/models/data-sources/_base.datasource';
import {ReportService} from '../../../services/report.service';

export class BookingOutStandingDataSource extends BaseDataSource {
    constructor(private reportService: ReportService) {
        super();
    }

    loadReportData(queryParams: any) {
        this.loadingSubject.next(true);
        this.reportService.bookingOutStanding(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadBookingReportData(queryParams: any) {
        this.loadingSubject.next(true);
        this.reportService.bookingReport(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }
}
