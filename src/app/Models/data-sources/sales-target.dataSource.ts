import {finalize, tap} from 'rxjs/operators';
import {BaseDataSource} from '../../content/pages/components/apps/e-commerce/_core/models/data-sources/_base.datasource';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {SalesTargetService} from '../../services/sales-target.service';

export class SalesTargetDataSource extends BaseDataSource {

    constructor(private salesTargetService: SalesTargetService) {
        super();
    }

    loadSalesTargets(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.salesTargetService.get(queryParams).pipe(
            tap(res => {
                const result: any = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }
}
