import {finalize, tap} from 'rxjs/operators';
import {BaseDataSource} from '../../content/pages/components/apps/e-commerce/_core/models/data-sources/_base.datasource';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {UserService} from '../../services/user.service';

export class UserDataSource extends BaseDataSource {

    constructor(protected userService: UserService) {
        super();
    }

    loadUsers(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.userService.getUsers(queryParams).pipe(
            tap(res => {
                const result: any = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }
}
