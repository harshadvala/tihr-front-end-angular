import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {AuthenticationService} from '../core/auth/authentication.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
	constructor(private authService: AuthenticationService) {
	}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// add authorization header with jwt token if available
		const accessToken = localStorage.getItem('accessToken');
		if (accessToken) {
			// let url = request.url;
			const authReq = request.clone({
				// url: url += '?token=' + accessToken,
				headers: request.headers.set('Authorization', 'Bearer ' + accessToken)
			});

            // return next.handle(authReq);
			return next.handle(authReq).pipe(catchError((error, caught) => {
				// intercept the response error and displace it to the console
                if (error.status === 401) { // navigate /delete cookies or whatever
                    console.log('handled error ' + error.status);
                    this.authService.logout(true);
                    location.reload(true);
                    return of(error);
                } else {
                    return next.handle(authReq);
                }
			}) as any);
		}

		return next.handle(request);
	}
}
