import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: 'app/content/pages/pages.module#PagesModule'
    },
    {
        path: '**',
        redirectTo: '404',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {enableTracing: false, useHash: true})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
