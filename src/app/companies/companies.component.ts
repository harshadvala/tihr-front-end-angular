import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {CompanyDetailComponent} from './company-detail/company-detail.component';
import {CompanyService} from '../services/company.service';
import {ToastrService} from 'ngx-toastr';
import {SelectionModel} from '@angular/cdk/collections';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {QueryParamsModel} from '../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {CompanyDataSource} from '../Models/data-sources/company.dataSource';
import {LayoutUtilsService, MessageType} from '../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {environment} from '../../environments/environment';

@Component({
    selector: 'm-companies',
    templateUrl: './companies.component.html',
    styleUrls: ['./companies.component.scss'],
    providers: [CompanyService]
})
export class CompaniesComponent implements OnInit {
    isCollapsedFilter: boolean = true;
    customFilters: any = {
        is_credit_list: '',
        credit_approved: ''
    };
    types: any[] = [];
    typeList: any = [];
    typeListSettings: any = {};
    selectedTypeLists = [];
    industryList: any = [];
    industryListSettings: any = {};
    selectedIndustryLists = [];
    categoryList: any = [];
    categoryListSettings: any = {};
    selectedCategoryLists = [];
    rateSourceList: any = [];
    rateSourceListSettings: any = {};
    selectedRateSourceLists = [];
    dataSource: any = CompanyDataSource;
    companySettingLists: any[] = [];
    companies: any[] = [];
    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    constructor(public dialog: MatDialog,
                private layoutUtilsService: LayoutUtilsService,
                public companyService: CompanyService,
                private toastService: ToastrService) {

        this.types = this.companyService.getCompanyTypes();

        const apiURL = environment.API_URL + '/companies/';

        this.typeListSettings = {
            singleSelection: false,
            text: 'Select Type',
            selectAllText: 'All',
            unSelectAllText: 'All',
            enableSearchFilter: false,
            classes: 'custom-dropdown-list',
            labelKey: 'name',
            badgeShowLimit: 1,
            primaryKey: 'id',
            maxHeight: 250
        };

        this.industryListSettings = {
            singleSelection: false,
            text: 'Select Industry',
            selectAllText: 'All',
            unSelectAllText: 'All',
            enableSearchFilter: false,
            classes: 'custom-dropdown-list',
            labelKey: 'name',
            badgeShowLimit: 1,
            primaryKey: 'id',
            maxHeight: 250
        };

        this.categoryListSettings = {
            singleSelection: false,
            text: 'Select Category',
            selectAllText: 'All',
            unSelectAllText: 'All',
            enableSearchFilter: false,
            classes: 'custom-dropdown-list',
            labelKey: 'name',
            badgeShowLimit: 1,
            primaryKey: 'id',
            maxHeight: 250
        };

        this.rateSourceListSettings = {
            singleSelection: false,
            text: 'Select Rate Source',
            selectAllText: 'All',
            unSelectAllText: 'All',
            enableSearchFilter: false,
            classes: 'custom-dropdown-list',
            labelKey: 'name',
            badgeShowLimit: 1,
            primaryKey: 'id',
            maxHeight: 250
        };

    }

    displayedColumns: string[] = [
        'select',
        'name',
        'person_name',
        'designation',
        'mobile',
        'email',
        'type',
        'city',
        'updated_at',
        'actions'
    ];

    ngOnInit() {
        this.loadCompanySettingList();

        this.paginator.page
            .pipe(
                tap(() => this.loadCompaniesList())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadCompaniesList();
                })
            )
            .subscribe();


        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadCompaniesList();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new CompanyDataSource(this.companyService);
        this.dataSource.loadCompanies(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.companies = res));
    }

    loadCompaniesList() {
        const queryParams = this.prepareFilterParameters();
        this.dataSource.loadCompanies(queryParams);
        this.selection.clear();
    }

    loadCompanySettingList() {
        this.companyService.getSettingList()
            .subscribe(response => {
                    this.companySettingLists = response;
                    this.typeList = this.types;
                    this.industryList = response.industries;
                    this.categoryList = response.categories;
                    this.rateSourceList = response.rateSources;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    openCompanyDetail(detail: any = {}) {
        const dialogRef = this.dialog.open(CompanyDetailComponent, {
            data: {
                currentRecord: detail,
                setting: this.companySettingLists
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadCompaniesList();
            }
        });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.companies.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.companies.length) {
            this.selection.clear();
        } else {
            this.companies.forEach(row => this.selection.select(row));
        }
    }

    deleteCompany(company) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.layoutUtilsService.showActionNotification('Not Implemented yet', MessageType.Delete);
        });
    }

    prepareFilterParameters() {
        const q = this.searchInput.nativeElement.value;
        let queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);

        queryParams = Object.assign(queryParams, this.customFilters);

        return queryParams;
    }

    applyFilter() {
        this.loadCompaniesList();
    }

    resetFilter() {
        this.selectedTypeLists = [];
        this.selectedIndustryLists = [];
        this.selectedCategoryLists = [];
        this.selectedRateSourceLists = [];
        this.customFilters = {
            industry_id: [],
            rate_source_id: [],
            category_id: [],
            type: [],
            is_credit_list: '',
            credit_approved: ''
        };

        this.loadCompaniesList();
    }

    onTypeSelect(item: any = {}) {
        this.customFilters.type = [];
        if (this.selectedTypeLists.length > 0) {
            this.selectedTypeLists.forEach(itm => {
                this.customFilters.type.push(itm.id);
            });
            this.customFilters.type = this.customFilters.type.join();
        }
    }

    onIndustrySelect(item: any = {}) {
        this.customFilters.industry_id = [];
        if (this.selectedIndustryLists.length > 0) {
            this.selectedIndustryLists.forEach(itm => {
                this.customFilters.industry_id.push(itm.id);
            });
            this.customFilters.industry_id = this.customFilters.industry_id.join();
        }
    }

    onCategorySelect(item: any = {}) {
        this.customFilters.category_id = [];
        if (this.selectedCategoryLists.length > 0) {
            this.selectedCategoryLists.forEach(itm => {
                this.customFilters.category_id.push(itm.id);
            });
            this.customFilters.category_id = this.customFilters.category_id.join();
        }
    }

    onRateSourceSelect(item: any = {}) {
        this.customFilters.rate_source_id = [];
        if (this.selectedRateSourceLists.length > 0) {
            this.selectedRateSourceLists.forEach(itm => {
                this.customFilters.rate_source_id.push(itm.id);
            });
            this.customFilters.rate_source_id = this.customFilters.rate_source_id.join();
        }
    }
}
