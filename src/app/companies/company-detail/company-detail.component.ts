import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {CommonService} from '../../services/common.service';
import {CompanyService} from '../../services/company.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {NgxPermissionsService} from 'ngx-permissions';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';

@Component({
    selector: 'm-company-detail',
    templateUrl: './company-detail.component.html',
    styleUrls: ['./company-detail.component.scss'],
    providers: [CommonService, CompanyService]
})
export class CompanyDetailComponent implements OnInit {
    loggedInUser: any = {};
    userPermissions: any = {};
    isSaving: boolean = false;
    isCreditApproveRight: boolean = false;
    companyRecord: any = {
        country_id: 63,
        state_id: 12,
        type: 0,
        room_inventory_type: 'Regular',
        rate_type: 'Regular'
    };
    countries: any[] = [];
    states: any[] = [];
    cities: any[] = [];
    types: any[] = [];
    roomInventories: any = [
        {
            id: 'Regular',
            name: 'Regular'
        },
        {
            id: 'Allocated',
            name: 'Allocated'
        }
    ];

    rateTypes: any[] = [
        {
            id: 'Regular',
            name: 'Regular'
        },
        {
            id: 'Special',
            name: 'Special'
        },
        {
            id: 'Allocated',
            name: 'Allocated'
        }
    ];

    industries: any[] = [];
    categories: any[] = [];
    rateSources: any[] = [];
    histories: any[] = [];

    constructor(public dialogRef: MatDialogRef<CompanyDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef,
                private toastService: ToastrService,
                private modalService: NgbModal,
                private layoutUtilsService: LayoutUtilsService,
                private permissions: NgxPermissionsService,
                private tokenStorage: TokenStorage,
                private companyService: CompanyService,
                private commonService: CommonService) {
        this.loggedInUser = this.tokenStorage.getUserDetail();
        this.userPermissions = this.permissions.getPermissions();
        if (this.data) {
            if (this.data.setting) {
                this.industries = this.data.setting.industries;
                this.categories = this.data.setting.categories;
                this.rateSources = this.data.setting.rateSources;
            }
            if (this.data.currentRecord.id) {
                this.companyRecord = this.data.currentRecord;
                this.loadCompanyHistory();

                if (!this.userPermissions['companies-update']) {
                    this.companyRecord.isReadOnly = true;
                }
            }
        }
        this.types = this.companyService.getCompanyTypes();

        const isGranted = this.loggedInUser.permissions.indexOf('company-credit-approve');
        if (isGranted >= 0) {
            this.isCreditApproveRight = true;
        }
    }

    ngOnInit() {
        this.loadCountries();
    }

    loadCountries() {
        this.commonService.getCountries()
            .subscribe(response => {
                    this.countries = response;
                    if (this.companyRecord.country_id) {
                        this.loadStates();
                    }
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    loadStates() {
        this.commonService.getStates(this.companyRecord.country_id)
            .subscribe(response => {
                    this.states = response;
                    if (this.companyRecord.state_id) {
                        this.loadCities();
                    }
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    loadCities() {
        this.commonService.getCities(this.companyRecord.state_id)
            .subscribe(response => {
                    this.cities = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    checkForDuplicateRecord() {
        this.isSaving = true;
        this.companyService.validateCompany(this.companyRecord)
            .subscribe(validate_response => {
                    if (Object.keys(validate_response).length > 0) {
                        this.isSaving = false;
                        const desc = this.prepareDuplicateRecordHtml(validate_response);
                        const dialogRef = this.layoutUtilsService.deleteElement(
                            'Found matched records, are you sure want to continue ?',
                            desc,
                            'Saving...',
                            'Yes',
                            'No',
                            {width: 'auto !important'}
                        );
                        dialogRef.afterClosed().subscribe(res => {
                            if (!res) {
                                return;
                            }
                            this.saveCompany();
                        });
                    } else {
                        this.isSaving = false;
                        this.saveCompany();
                    }
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.cdr.detectChanges();
                });
    }

    prepareDuplicateRecordHtml(validate_response) {
        let desc: string = '<table class="table table-bordered">' +
            '<thead>' +
            '<th><b>No.</b></th>' +
            '<th><b>Name</b></th>' +
            '<th><b>Type</b></th>' +
            '<th><b>Email</b></th>' +
            '<th><b>Phone</b></th>' +
            '<th><b>Mobile</b></th>' +
            '<th><b>City</b></th>' +
            '</thead>';

        let no = 1;
        validate_response.forEach((keys: any, values: any) => { // console.log(values);
            const type = this.types.find(item => item.id === keys.type);

            keys.phone = (keys.phone) ? keys.phone : '';
            keys.email = (keys.email) ? keys.email : '';
            keys.city = (keys.city) ? keys.city : '';
            keys.mobile = (keys.mobile) ? keys.mobile : '';

            desc += '<tbody>' +
                '<td> ' + no++ + ' </td>' +
                '<td> ' + keys.name + ' </td>' +
                '<td> ' + type.name + ' </td>' +
                '<td> ' + keys.email + ' </td>' +
                '<td> ' + keys.phone + ' </td>' +
                '<td> ' + keys.mobile + ' </td>' +
                '<td> ' + keys.city + ' </td>' +
                '</tbody>';
        });

        desc += '</table>';

        return desc;
    }

    saveCompany() {
        this.isSaving = true;
        if (!this.companyRecord.is_credit_list) {
            this.companyRecord.credit_days = null;
            this.companyRecord.credit_approved = false;
            this.companyRecord.credit_approved_by = '';
        }
        this.companyService.saveCompany(this.companyRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.cdr.detectChanges();
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.cdr.detectChanges();
                });
    }

    loadCompanyHistory() {
        this.companyService.getHistory(this.companyRecord.id)
            .subscribe(response => {
                    this.histories = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    openHistory(content) {
        this.modalService.open(content).result.then((result) => {
            // this.closeResult = `Closed with: ${result}`;
        });
    }
}

