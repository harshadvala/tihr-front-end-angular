import {ChangeDetectorRef, Component, HostBinding, Input, OnInit} from '@angular/core';
import {CommonService} from '../../../../../services/common.service';

@Component({
    selector: 'm-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
    providers: [CommonService],
})
export class NotificationComponent implements OnInit {
    notificationList: any[] = [];
    isOpenedNotification = false;
    numberOfTicks = 0;
    @HostBinding('class')
        // tslint:disable-next-line:max-line-length
    classes = 'm-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width';

    @HostBinding('attr.m-dropdown-toggle') attrDropdownToggle = 'click';
    @HostBinding('attr.m-dropdown-persistent') attrDropdownPersisten = 'true';

    @Input() animateShake: any;
    @Input() animateBlink: any;

    constructor(private ref: ChangeDetectorRef, private commonService: CommonService) {
        // animate icon shake and dot blink
        setInterval(() => {
            if (this.notificationList.length > 0 && !this.isOpenedNotification) {
                this.animateShake = 'm-animate-shake';
                this.animateBlink = 'm-animate-blink';
            } else {
                this.animateShake = this.animateBlink = '';
            }
        }, 5000); // 3000
        setInterval(() => (this.animateShake = this.animateBlink = ''), 4000); // 6000

        setInterval(() => {
            this.numberOfTicks++;
            this.ref.markForCheck();
        }, 1000);

        this.getNotifications();
    }

    ngOnInit(): void {
    }

    getNotifications() {
        this.commonService.getNotifications()
            .subscribe(response => {
                    this.notificationList = response;
                },
                error => {
                    //
                });
    }

}
