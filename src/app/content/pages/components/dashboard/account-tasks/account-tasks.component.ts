import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {environment} from '../../../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material';
import {CommonService} from '../../../../../services/common.service';
import {DashboardService} from '../../../../../services/dashboard.service';
import {InqPaymentConfirmComponent} from '../../../../../inquiry/inq-payment-confirm/inq-payment-confirm.component';
import {UtilsService} from '../../../../../core/services/utils.service';

@Component({
    selector: 'm-account-tasks',
    templateUrl: './account-tasks.component.html',
    styleUrls: ['./account-tasks.component.scss'],
    providers: [DashboardService, CommonService, UtilsService]
})
export class AccountTasksComponent implements OnInit {
    isLoading: boolean = false;
    tasks: any[] = [];
    numberOfTicks = 0;
    viewTasks: any = [1];
    baseUrl: any = environment.Base_Url;
    taskTypes: any[] = [];

    hotelIds: any[] = [];
    hotelList: any = [];
    hotelListSettings: any = {};
    selectedHotelLists = [];

    constructor(private toastService: ToastrService,
                public dialog: MatDialog,
                private ref: ChangeDetectorRef,
                private commonService: CommonService,
                private utilsService: UtilsService,
                private dashboardService: DashboardService) {

        setInterval(() => {
            this.numberOfTicks++;
            this.ref.markForCheck();
        }, 1000);

        this.hotelListSettings = this.utilsService.getMultiSelctConfig({
            text: 'Select Hotels',
            labelKey: 'code',
            groupBy: 'status',
            maxHeight: 230
        });
    }

    ngOnInit() {
        this.getPaymentModes();
        this.getHotels();
    }

    loadTasks() {
        this.isLoading = true;
        this.dashboardService.tasks({types: this.viewTasks, hotelIds: this.hotelIds})
            .subscribe(response => {
                    this.isLoading = false;
                    this.tasks = response;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    openInquiryForm(record) {
        const dialogRef = this.dialog.open(InqPaymentConfirmComponent, {
            width: '350px',
            data: {
                paymentDetail: record
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            record.selected = false;
            if (result) {
                this.loadTasks();
            }
        });
    }

    getPaymentModes() {
        this.commonService.getPaymentModes()
            .subscribe(response => {
                    const sp = response.find(s => s.name === 'Balance at Unit');
                    if (sp) {
                        this.taskTypes = [sp];
                        this.viewTasks = [this.taskTypes[0]['id']];
                        this.loadTasks();
                    }
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    applyFilter() {
        this.loadTasks();
    }

    getHotels() {
        this.dashboardService.getInquirySettingLists()
            .subscribe(response => {
                    this.hotelList = response.hotels;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    onHotelSelect(item: any = {}) {
        this.hotelIds = [];
        if (this.selectedHotelLists.length > 0) {
            this.selectedHotelLists.forEach(itm => {
                this.hotelIds.push(itm.id);
            });
        }
    }
}
