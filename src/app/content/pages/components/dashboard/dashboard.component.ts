import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {LayoutConfigService} from '../../../../core/services/layout-config.service';
import {ToastrService} from 'ngx-toastr';
import {DashboardService} from '../../../../services/dashboard.service';
import * as moment from 'moment';
import {TokenStorage} from '../../../../core/auth/token-storage.service';
import {UtilsService} from '../../../../core/services/utils.service';
import {InquiryService} from '../../../../inquiry/inquiry.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'm-dashboard',
    templateUrl: './dashboard.component.html',
    providers: [DashboardService, UtilsService, InquiryService]
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
    loggedInUser: any = {};
    numberOfTicks = 0;
    isInitilized: boolean = false;
    chartSettings: any = {fromDate: moment().subtract(29, 'days').format('YYYY-MM-DD'), toDate: moment().format('YYYY-MM-DD')};

    dateOptions: any = {};
    inquiryReminders: any = [];

    @ViewChild('inquiryReminderModal') inquiryReminderModal;

    constructor(
        private router: Router,
        private configService: LayoutConfigService,
        private toastService: ToastrService,
        private dashboardService: DashboardService,
        private ref: ChangeDetectorRef,
        private modalService: NgbModal,
        private utilsService: UtilsService,
        private inquiryService: InquiryService,
        private tokenStorage: TokenStorage
    ) {

        this.chartSettings.fromDate = moment().subtract(29, 'days').format('YYYY-MM-DD');
        this.chartSettings.toDate = moment().format('YYYY-MM-DD');

        this.loggedInUser = this.tokenStorage.getUserDetail();

        setInterval(() => {
            this.numberOfTicks++;
            this.ref.markForCheck();
        }, 1000);

        this.isInitilized = true;
        this.dateOptions = this.utilsService.getCalenderConfig();

    }

    ngOnInit(): void {
        this.loadInquiryReminders();
    }

    selectedDate(value: any) {
        console.log(value);
        this.chartSettings.fromDate = value.start.format('YYYY-MM-DD');
        this.chartSettings.toDate = value.end.format('YYYY-MM-DD');
    }

    loadInquiryReminders() {
        this.inquiryService.getUserInquiryReminders()
            .subscribe(response => {
                    this.inquiryReminders = response;
                    if (this.inquiryReminders.length > 0) {
                        this.openInquiryReminders();
                    }
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    openInquiryReminders() {
        this.modalService.open(this.inquiryReminderModal, {centered: true});
    }

    updateReminderStatus(reminder: any = {}, status: any = '') {
        reminder.updating = true;
        this.inquiryService.updateInquiryReminderStatus(reminder.reminder_id, status)
            .subscribe(response => {
                    reminder.updating = false;
                    this.loadInquiryReminders();
                },
                error => {
                    reminder.updating = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }
}
