import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {SettingsService} from '../../../../../services/settings.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
    selector: 'm-finance-dashboard',
    templateUrl: './finance-dashboard.component.html',
    styleUrls: ['./finance-dashboard.component.scss'],
    providers: [SettingsService]
})
export class FinanceDashboardComponent implements OnInit {
    isLoading: boolean = false;
    url: SafeResourceUrl;

    constructor(public sanitizer: DomSanitizer,
                private route: ActivatedRoute,
                public  settingsService: SettingsService) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe((
            params: ParamMap) => {
            const slug = params.get('slug');

            this.settingsService.dynamicDashboardsDetails({ 'slug': slug })
                .subscribe((response: any) => {
                    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(response.url);
                });

        });
    }

}
