import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {DashboardService} from '../../../../../services/dashboard.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'm-sales-outstanding-chart',
    templateUrl: './sales-outstanding-chart.component.html',
    styleUrls: ['./sales-outstanding-chart.component.scss'],
    providers: [DashboardService]
})
export class SalesOutstandingChartComponent implements OnChanges {

    chartSettings: any = {fromDate: '', toDate: ''};
    @Input() fromDate: any;
    @Input() toDate: any;

    hotelChartConfig: any = {
        hotelOutStandingByValue: 'inquiry_date',
        groupBy: 'hotel'
    };

    public chartLabels: string[] = [];
    public barChartLegend: boolean = true;
    public outStandingChartData: any[] = [];
    isLoadingSaleOutstanding: boolean = false;

    hotelOutStandingBy: any = [
        {
            name: 'By Inquiry Date',
            value: 'inquiry_date'
        },
        {
            name: 'By Checkin Date',
            value: 'check_in_date'
        }
    ];

    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        maintainAspectRatio: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,

                },
                stacked: true
            }],
            xAxes: [{
                beginAtZero: true,
                stacked: true,
                ticks: {
                    autoSkip: false,
                    maxRotation: 90,
                }
            }]
        }
    };

    colors = [];

    constructor(private toastService: ToastrService, private dashboardService: DashboardService) {
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(changes);
        let dateChanged = false;
        if (changes.fromDate) {
            this.chartSettings.fromDate = changes.fromDate.currentValue;
            dateChanged = true;
        }

        if (changes.toDate) {
            this.chartSettings.toDate = changes.toDate.currentValue;
            dateChanged = true;
        }

        if (dateChanged) {
            this.hotelOutStanding();
        }
    }

    hotelOutStanding() {
        this.isLoadingSaleOutstanding = true;
        this.dashboardService.hotelOutstanding(Object.assign({
            byDate: this.hotelChartConfig.hotelOutStandingByValue,
            groupBy: this.hotelChartConfig.groupBy
        }, this.chartSettings))
            .subscribe(response => {
                    this.outStandingChartData = [];
                    if (response.length > 0) {
                        this.chartLabels = [];
                        let index = 0;
                        response.forEach(element => {
                            this.chartLabels.push(element.label);
                            if (index === 0) {
                                this.colors = element.colors;
                                this.outStandingChartData = [];
                                let chartDataObj;
                                element.data.forEach(values => {
                                    chartDataObj = {data: [], label: ''};
                                    chartDataObj.label = values.name;
                                    let statusValues;

                                    response.forEach(groupData => {
                                        statusValues = groupData.data.find(item => item.name === values.name);
                                        if (statusValues) {
                                            chartDataObj.data.push(statusValues.value);
                                        }
                                    });
                                    this.outStandingChartData.push(Object.assign({}, chartDataObj));
                                });
                            }
                            index++;
                        });
                    }
                    this.isLoadingSaleOutstanding = false;
                },
                error => {
                    this.isLoadingSaleOutstanding = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    changeHotelStateByDate(type) {
        this.hotelChartConfig.hotelOutStandingByValue = type;
        this.hotelOutStanding();
    }

    changeGroupBy(type) {
        this.hotelChartConfig.groupBy = type;
        this.hotelOutStanding();
    }
}
