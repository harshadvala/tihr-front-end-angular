import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TokenStorage} from '../../../../core/auth/token-storage.service';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../../../services/user.service';
import {LayoutUtilsService} from '../../components/apps/e-commerce/_core/utils/layout-utils.service';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'm-profile',
    templateUrl: './profile.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [UserService]
})
export class ProfileComponent implements OnInit {

    isSaving: boolean = false;
    hidePassword: boolean = true;
    userDetail: any = {
        status: 1,
        ota_booking_assign: false,
        image: ''
    };
    origUserDetail: any = {
        status: 1,
        ota_booking_assign: false,
        image: ''
    };
    genders: any[] = [
        {
            id: 'Male',
            name: 'Male'
        },
        {
            id: 'Female',
            name: 'Female'
        }
    ];
    // Attachment
    afuConfig: any = {};

    constructor(private tokenStorage: TokenStorage,
                private toastService: ToastrService,
                private cdr: ChangeDetectorRef,
                private userService: UserService,
                private layoutUtilsService: LayoutUtilsService) {
    }

    ngOnInit() {
        this.userDetail = this.tokenStorage.getUserDetail();
        this.origUserDetail = this.tokenStorage.getUserDetail();
        this.initAttachmentConfig(this.userDetail.id);
    }

    initAttachmentConfig(user_id: any = '') {
        const accessToken = localStorage.getItem('accessToken');
        this.afuConfig = {
            multiple: false,
            formatsAllowed: '.jpg,.png', /*,.pdf,.docx, .txt,.gif,.jpeg,.xls,.xlsx*/
            maxSize: '2',
            uploadAPI: {
                url: this.userService.getUserImageUploadUrl(user_id),
                headers: {
                    'Authorization': 'Bearer ' + accessToken
                }
            },
            theme: 'attachPin',
            hideProgressBar: true,
            hideResetBtn: true,
            hideSelectBtn: true,
            attachPinText: ' '
        };
    }

    userFileUploaded(response) {
        const userRec = JSON.parse(response.response);
        this.userDetail.image = userRec.image;
        this.cdr.detectChanges();
    }

    deleteUserImage(userDetail) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.userService.deleteUserImage(userDetail.id)
                .subscribe(response => {
                        console.log(response);
                        this.userDetail.image = response.image;
                        this.cdr.detectChanges();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

    resetUser() {
        // console.log(this.userDetail);
        // console.log(this.origUserDetail);
        this.userDetail.mobile = this.origUserDetail.mobile;
        this.userDetail.dob = this.origUserDetail.dob;
        this.userDetail.gender = this.origUserDetail.gender;
    }

    saveUser() {
        this.isSaving = true;
        this.userDetail.name = this.userDetail.first_name + ' ' + this.userDetail.last_name;
        this.userService.saveProfile(this.userDetail)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    dobDateChangeEvent(type: string) {
        this.userDetail[type] = this.userDetail[type].format('YYYY-MM-DD HH:mm:ss');
    }
}
