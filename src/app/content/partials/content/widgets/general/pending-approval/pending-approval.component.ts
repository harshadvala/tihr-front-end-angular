import {Component, OnInit} from '@angular/core';
import {DashboardService} from '../../../../../../services/dashboard.service';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material';
import {environment} from '../../../../../../../environments/environment';
import {CommonService} from '../../../../../../services/common.service';
import {TokenStorage} from '../../../../../../core/auth/token-storage.service';
import {UtilsService} from '../../../../../../core/services/utils.service';
import {InqDiscountApproveComponent} from '../../../../../../inquiry/inq-discount-approve/inq-discount-approve.component';

@Component({
    selector: 'm-pending-approval',
    templateUrl: './pending-approval.component.html',
    providers: [DashboardService, CommonService, UtilsService]
})
export class PendingApprovalComponent implements OnInit {
    isLoading: boolean = false;
    pendingApproval: any[] = [];
    baseUrl: any = environment.Base_Url;

    discountApprovalValidationRule: any;

    loggedInUser: any = {};

    constructor(private toastService: ToastrService,
                public dialog: MatDialog,
                private commonService: CommonService,
                private utilsService: UtilsService,
                private dashboardService: DashboardService,
                private tokenStorage: TokenStorage) {
        this.loggedInUser = this.tokenStorage.getUserDetail();
        this.discountApprovalValidationRule = this.tokenStorage.getAppConfigByKey('discount_approval_validation_rule');
    }

    ngOnInit() {
        this.loadPendingApproval();
    }

    loadPendingApproval() {
        this.isLoading = true;
        this.dashboardService.pendingApproval(
            {
                userId: this.loggedInUser.id,
            })
            .subscribe(response => {
                    this.isLoading = false;
                    this.pendingApproval = response;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    openPendingApprovalForm(record) {
        const dialogRef = this.dialog.open(InqDiscountApproveComponent, {
            width: '400px',
            data: {
                bookingDetail: record
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            record.selected = false;
            if (result) {
                this.loadPendingApproval();
            }
        });
    }
}
