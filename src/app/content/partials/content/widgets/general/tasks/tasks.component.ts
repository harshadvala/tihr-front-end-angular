import {Component, OnInit} from '@angular/core';
import {DashboardService} from '../../../../../../services/dashboard.service';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material';
import {InqPaymentConfirmComponent} from '../../../../../../inquiry/inq-payment-confirm/inq-payment-confirm.component';
import {environment} from '../../../../../../../environments/environment';
import {CommonService} from '../../../../../../services/common.service';
import * as moment from 'moment';
import {TokenStorage} from '../../../../../../core/auth/token-storage.service';
import {UtilsService} from '../../../../../../core/services/utils.service';
import {AddInquiryComponent} from '../../../../../../inquiry/add-inquiry/add-inquiry.component';
import {InquiryService} from '../../../../../../inquiry/inquiry.service';

@Component({
    selector: 'm-tasks',
    templateUrl: './tasks.component.html',
    providers: [DashboardService, CommonService, UtilsService]
})
export class TasksComponent implements OnInit {
    fromDate = moment().subtract(29, 'days').format('YYYY-MM-DD');
    toDate = moment().format('YYYY-MM-DD');
    isLoading: boolean = false;
    tasks: any[] = [];
    viewTasks: any[] = [1, 3];
    baseUrl: any = environment.Base_Url;
    taskTypes: any[] = [];
    yearStartDate = moment().startOf('year');
    dateOptions: any = {};
    selectedAll: boolean = false;

    hotelIds: any[] = [];
    hotelList: any = [];
    hotelListSettings: any = {};
    selectedHotelLists = [];
	inquirySettings: any = {};

    constructor(private toastService: ToastrService,
                public dialog: MatDialog,
                private commonService: CommonService,
                private utilsService: UtilsService,
                private dashboardService: DashboardService,
				private inquiryService: InquiryService,
                private tokenStorage: TokenStorage) {

        const yearStart: any = this.tokenStorage.getAppConfigByKey('financial_year_start_date');
        if (yearStart) {
            this.yearStartDate = moment(yearStart, 'YYYY-MM-DD');
        }

        this.dateOptions = this.utilsService.getCalenderConfig();

        this.hotelListSettings = this.utilsService.getMultiSelctConfig({
            text: 'Select Hotels',
            labelKey: 'code',
            groupBy: 'status',
            maxHeight: 230
        });
    }

    ngOnInit() {
		this.getInquirySettingLists();
        this.getPaymentModes();
        this.getHotels();
        // this.loadTasks();
    }

    loadTasks() {
        this.isLoading = true;
        this.dashboardService.tasks(
            {
                types: this.viewTasks,
                fromDate: this.fromDate,
                toDate: this.toDate,
                hotelIds: this.hotelIds
            })
            .subscribe(response => {
                    this.isLoading = false;
                    this.tasks = response;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    openInquiryForm(record) {
        const dialogRef = this.dialog.open(InqPaymentConfirmComponent, {
            width: '350px',
            data: {
                paymentDetail: record
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            record.selected = false;
            if (result) {
                this.loadTasks();
            }
        });
    }

	moreActionPerform(record: any = {}) {
		const dialogRef = this.dialog.open(AddInquiryComponent, {
			data: {
				statuses: this.inquirySettings.statuses,
				sources: this.inquirySettings.sources,
				assignToList: this.inquirySettings.users,
				subStatuses:this.inquirySettings.subStatuses,
				paymentStatuses: this.inquirySettings.inquiryPaymentStatuses,
				paymentSubStatuses: this.inquirySettings.inquiryPaymentSubStatuses,
				types: this.inquirySettings.types,
				hotels: this.inquirySettings.hotels,
				travel_agents: this.inquirySettings.travel_agents,
				companies: this.inquirySettings.companies,
				currentRecord: record,
				originalBooking: Object.assign({}, record),
				params: {selectTab: 1},
				detailViewType: 1
			}
		});
		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.applyFilter();
			}
		});
	}

	getInquirySettingLists() {
		this.inquiryService.getInquirySettingLists()
			.subscribe(response => {
					this.inquirySettings = response;
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}

    getPaymentModes() {
        this.commonService.getPaymentModes()
            .subscribe(response => {
                    this.taskTypes = response;
                    this.taskTypes.push({
                        id: 'rejected',
                        name: 'Rejected Payments'
                    });

                    this.selectAllFilter();
                    this.loadTasks();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    selectedDate(value: any) {
        this.fromDate = value.start.format('YYYY-MM-DD');
        this.toDate = value.end.format('YYYY-MM-DD');
        // this.loadTasks();
    }

    applyFilter() {
        this.loadTasks();
    }

    selectAllFilter() {
        if (this.viewTasks.length === this.taskTypes.length) {
            this.taskTypes.forEach(stat => {
                stat.is_selected = false;
            });
        } else {
            this.taskTypes.forEach(stat => {
                stat.is_selected = true;
            });
        }
        this.statusSelectChange();
    }

    statusSelectChange() {
        const selectedTasks = this.taskTypes.filter(item => {
            return item.is_selected;
        });

        this.viewTasks = [];
        selectedTasks.forEach(itm => {
            this.viewTasks.push(itm.id);
        });

        if (this.viewTasks.length === this.taskTypes.length) {
            this.selectedAll = true;
        } else {
            this.selectedAll = false;
        }
    }

    getHotels() {
        this.dashboardService.getInquirySettingLists()
            .subscribe(response => {
                    this.hotelList = response.hotels;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    onHotelSelect(item: any = {}) {
        this.hotelIds = [];
        if (this.selectedHotelLists.length > 0) {
            this.selectedHotelLists.forEach(itm => {
                this.hotelIds.push(itm.id);
            });
        }
    }
}
