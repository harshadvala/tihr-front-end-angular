import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {LogsService} from '../../../../../core/services/logs.service';
import {LogData} from '../../../../../core/interfaces/log-data';
import {Observable} from 'rxjs';
import {CommonService} from '../../../../../services/common.service';

@Component({
    selector: 'm-list-timeline',
    templateUrl: './list-timeline.component.html',
    providers: [CommonService],
})
export class ListTimelineComponent implements OnInit {
    notificationList: any[] = [];
    numberOfTicks = 0;
    @Input() type: any;
    @Input() heading: any;

    @Input() logList: Observable<LogData[]>;

    constructor(private ref: ChangeDetectorRef,private commonService: CommonService, private logsService: LogsService) {
        this.getNotifications();
    }

    ngOnInit() {
        // this.logList = this.logsService.getData({ types: this.type });
    }

    getNotifications() {
        this.commonService.getNotifications()
            .subscribe(response => {
                    this.notificationList = response;
                },
                error => {
                    //
                });
    }
}
