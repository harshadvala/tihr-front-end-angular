import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTimelineComponent } from './list-timeline.component';
import { TimelineItemComponent } from './timeline-item/timeline-item.component';
import { CoreModule } from '../../../../../core/core.module';
import {MatTooltipModule} from '@angular/material';

@NgModule({
    imports: [CommonModule, CoreModule, MatTooltipModule],
	declarations: [ListTimelineComponent, TimelineItemComponent],
	exports: [ListTimelineComponent, TimelineItemComponent]
})
export class ListTimelineModule {}
