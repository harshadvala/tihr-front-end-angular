import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ListTimelineComponent} from '../list-timeline.component';
import {CommonService} from '../../../../../../services/common.service';
import {NotificationComponent} from '../../../../../layout/header/topbar/notification/notification.component';

@Component({
    selector: 'm-timeline-item',
    templateUrl: './timeline-item.component.html',
    styleUrls: ['./timeline-item.component.scss']
})
export class TimelineItemComponent implements OnInit {
    isProcessing: boolean = false;

    @Input() item: any;

    @HostBinding('class') classes = 'm-list-timeline__item';

    constructor(private router: Router, private commonService: CommonService,
                private listTimeLineComponents: ListTimelineComponent,
                private notificationComponents: NotificationComponent) {
    }

    ngOnInit() {
        if (this.item.read) {
            this.classes += ' m-list-timeline__item--read';
        }
    }

    badgeClass() {
        const badges: any = {
            urgent: 'm-badge--info',
            important: 'm-badge--warning',
            resolved: 'm-badge--success',
            pending: 'm-badge--danger'
        };
        if (this.item.tags.length > 0) {
            return badges[this.item.tags[0]];
        }
    }

    redirectToBooking(id) {
        this.router.navigate(['/booking/' + id]);
    }

    redirectPayment(id, paymentId) {
        this.router.navigate(['/booking/' + id + '&payment=' + paymentId]);
    }

    archiveNotification(id) {
        this.isProcessing = true;
        this.commonService.archiveNotification(id)
            .subscribe(response => {
                this.listTimeLineComponents.getNotifications();
                this.notificationComponents.getNotifications();
            });
    }
}
