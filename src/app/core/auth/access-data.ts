export interface AccessData {
	accessToken: string;
	refreshToken: string;
	roles: any;
	permissions:any;
	user:any;
}
