import {Injectable} from '@angular/core';
import {HttpHeaders, HttpParams} from '@angular/common/http';
import * as moment from 'moment';
import {TokenStorage} from '../auth/token-storage.service';

@Injectable()
export class UtilsService {
    constructor(private tokenStorage: TokenStorage) {
    }

    /**
     * Build url parameters key and value pairs from array or object
     * @param obj
     */
    urlParam(obj: any): string {
        return Object.keys(obj)
            .map(k => k + '=' + encodeURIComponent(obj[k]))
            .join('&');
    }

    /**
     * Simple object check.
     * @param item
     * @returns {boolean}
     */
    isObject(item) {
        return item && typeof item === 'object' && !Array.isArray(item);
    }

    /**
     * Deep merge two objects.
     * @param target
     * @param ...sources
     * @see https://stackoverflow.com/a/34749873/1316921
     */
    mergeDeep(target, ...sources) {
        if (!sources.length) {
            return target;
        }
        const source = sources.shift();

        if (this.isObject(target) && this.isObject(source)) {
            for (const key in source) {
                if (this.isObject(source[key])) {
                    if (!target[key]) {
                        Object.assign(target, {[key]: {}});
                    }
                    this.mergeDeep(target[key], source[key]);
                } else {
                    Object.assign(target, {[key]: source[key]});
                }
            }
        }

        return this.mergeDeep(target, ...sources);
    }

    getPath(obj, val, path?) {
        path = path || '';
        let fullpath = '';
        for (const b in obj) {
            if (obj[b] === val) {
                return path + '/' + b;
            } else if (typeof obj[b] === 'object') {
                fullpath =
                    this.getPath(obj[b], val, path + '/' + b) || fullpath;
            }
        }
        return fullpath;
    }

    getFindHTTPParams(queryParams): HttpParams {
        const params = new HttpParams()
            .set('lastNamefilter', queryParams.filter)
            .set('sortOrder', queryParams.sortOrder)
            .set('sortField', queryParams.sortField)
            .set('pageNumber', queryParams.pageNumber.toString())
            .set('pageSize', queryParams.pageSize.toString());

        return params;
    }

    getHTTPHeader() {
        return {
            headers: new HttpHeaders({'Content-Type': 'application/json'})
        };
    }

    getCKEditorConfig(params: any = {}) {
        let config: any = {
            forceEnterMode: true,
            language_list: ['en:English'],
            height: 350,
            toolbarStartupExpanded: false,
            toolbarGroups: [
                {name: 'others'},
                {name: 'clipboard', groups: ['clipboard', 'undo']},
                {name: 'editing', groups: ['find', 'selection', 'spellchecker']},
                {name: 'links'},
                {name: 'insert'},
                {name: 'forms'},
                {name: 'styles'},
                '/',
                {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi']},
                {name: 'colors'},
                {name: 'document', groups: ['document', 'doctools', 'mode']},
                {name: 'tools'},
            ]
        };

        return config = Object.assign(config, params);

    }

    getTinyConfig(params: any = {}, type: string = '') {
        let config: any = {
            selector: 'textarea',
            height: 350,
            theme: 'modern',
            plugins: 'print preview fullpage powerpaste searchreplace' +
                ' autolink directionality advcode visualblocks visualchars fullscreen ' +
                'image link media codesample table charmap hr pagebreak nonbreaking anchor toc ' +
                'insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker ' +
                'imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor |' +
                ' link | alignleft aligncenter alignright alignjustify ' +
                ' | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        };

        if (type === 'basic') {
            config.menubar = false;
            config.plugins = [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ];
            config.toolbar1 = 'insert | undo redo |  formatselect | bold italic backcolor ' +
                ' | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat';
        }

        return config = Object.assign(config, params);

    }

    getCalenderConfig(config: any = {}) {
        let yearStartDate: any = moment().startOf('year');
        const yearStart: any = this.tokenStorage.getAppConfigByKey('financial_year_start_date');
        if (yearStart) {
            yearStartDate = moment(yearStart, 'YYYY-MM-DD');
        }

        let dateOptions = {
            locale: {format: 'DD-MM-YYYY'},
            alwaysShowCalendars: false,
            opens: 'left',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Till Today': [yearStartDate, moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            }
        };

        dateOptions = Object.assign(dateOptions, config);
        return dateOptions;
    }


    getMultiSelctConfig(config: any = {}) {
        let configuration = {
            singleSelection: false,
            text: 'Select',
            selectAllText: 'All',
            unSelectAllText: 'All',
            enableSearchFilter: true,
            classes: 'custom-dropdown-list',
            labelKey: 'name',
            badgeShowLimit: 2,
            primaryKey: 'id',
            maxHeight: 250,
            selectGroup: true
        };

        configuration = Object.assign(configuration, config);
        return configuration;
    }
}


export function isInteger(value: any): value is number {
    return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
}


export function isString(value: any): value is string {
    return typeof value === 'string';
}
