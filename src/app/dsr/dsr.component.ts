import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {LayoutUtilsService, MessageType} from '../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {CompanyService} from '../services/company.service';
import {ToastrService} from 'ngx-toastr';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {QueryParamsModel} from '../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {DsrDetailComponent} from './dsr-detail/dsr-detail.component';
import {DSRDataSource} from '../Models/data-sources/dsr.dataSource';
import {DsrService} from '../services/dsr.service';
import {TokenStorage} from '../core/auth/token-storage.service';

@Component({
    selector: 'm-dsr',
    templateUrl: './dsr.component.html',
    styleUrls: ['./dsr.component.scss'],
    providers: [DsrService, CompanyService]
})
export class DsrComponent implements OnInit {

    dataSource: any = DSRDataSource;
    companySettingLists: any[] = [];
    companies: any[] = [];
    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;
    loggedInUser: any = {};

    constructor(public dialog: MatDialog,
                private layoutUtilsService: LayoutUtilsService,
                public companyService: CompanyService,
                public dsrService: DsrService,
                private tokenStorage: TokenStorage,
                private toastService: ToastrService) {
        this.loggedInUser = this.tokenStorage.getUserDetail();
    }

    displayedColumns: string[] = [
        'select',
        'company_name',
        'assign_to_name',
        'visited_date',
        'visited_time',
        'city',
        'person_name',
        'mobile',
        'email',
        // 'type',
        'actions'
    ];

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadDSRList())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadDSRList();
                })
            )
            .subscribe();


        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadDSRList();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new DSRDataSource(this.dsrService);
        this.dataSource.loadDSRs(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.companies = res));
    }

    loadDSRList() {
        const q = this.searchInput.nativeElement.value;
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadDSRs(queryParams);
        this.selection.clear();
    }

    openDSRDetail(detail: any = {}) {
        const dialogRef = this.dialog.open(DsrDetailComponent, {
            data: {
                currentRecord: detail,
                setting: this.companySettingLists
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadDSRList();
            }
        });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.companies.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.companies.length) {
            this.selection.clear();
        } else {
            this.companies.forEach(row => this.selection.select(row));
        }
    }

    deleteDRS(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.layoutUtilsService.showActionNotification('Not Implemented yet', MessageType.Delete);
        });
    }

    hours_am_pm(time) {
        if (time) {
            let hours = time[0] + time[1];
            let min = time[3] + time[4];
            if (hours < 12) {
                return hours + ':' + min + ' AM';
            } else {
                hours = hours - 12;
                hours = (hours.length < 10) ? '0' + hours : hours;
                return hours + ':' + min + ' PM';
            }
        }
        return '';
    }
}

