import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {InquiryService} from '../inquiry.service';
import {SpinnerButtonOptions} from '../../content/partials/content/general/spinner-button/button-options.interface';
import * as moment from 'moment';
import {InquiryModel} from '../../Models/InquiryModel';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {FormControl, Validators} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {CompanyService} from '../../services/company.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {CommonService} from '../../services/common.service';
import {Router} from '@angular/router';
import {NgxPermissionsService} from 'ngx-permissions';

let apiURL;
let companySearchParams: any = {};

@Component({
	selector: 'm-add-inquiry',
	templateUrl: './add-inquiry.component.html',
	styleUrls: ['./add-inquiry.component.scss'],
	providers: [InquiryService, CompanyService, CommonService]
})

export class AddInquiryComponent implements OnInit {
	serverUrl = environment.Server_URL;
	detailViewType: any = 0;
	searchTerm: any = '';
	reject_note: any = '';
	viewPaymentForm: boolean = false;
	selectedTabIndex: number = 0;
	/*0 - Details  1 - Notes  2 - Payments*/
	statuses: any = [];
	currentStatus: any = {};
	currentSource: any = {};
	currentInqType: any = {};
	currentPaymentMode: any = {};
	currentBank: any = {};
	sources: any = [];
	assignToList: any = [];
	subStatuses: any = [];
	types: any = [];
	hotels: any = [];
	travelAgents: any = [];
	companies: any = [];
	newInquiry: InquiryModel;
	maxDate: any = '';
	checkInMinDate: any = '';
	checkOutMinDate: any = '';
	minInqDate: any = moment().subtract(6, 'months');
	isVisibleCompanies: any = false;
	isVisibleOTA: any = false;
	isVisibleAgents: any = false;
	sendingEmail: any = false;
	events: any = [];
	histories: any = [];
	originalInquiry: any = {};
	isSaving: boolean = false;
	sendingPaymentRequest: boolean = false;
	additionalParams: any = {};
	spinner: SpinnerButtonOptions = {
		active: false,
		spinnerSize: 18,
		raised: true,
		buttonColor: 'primary',
		spinnerColor: 'accent',
		fullWidth: false
	};
	loggedInUser: any = {};
	isPaymentConfirmRight: boolean = false;
	allowRemoveInvoice: boolean = false;
	travel_agent = new FormControl(null, [Validators.required]);
	travelAgentDataSource: DataSource;

	ota = new FormControl(null, [Validators.required]);
	otaDataSource: DataSource;

	companyList = new FormControl(null, [Validators.required]);
	companyDataSource: DataSource;
	companyListArray: any[] = [];

	// Payments
	isLoadingPayments: boolean = false;
	isDeletePayment: boolean = false;
	paymentModes: any = [];
	paymentEntry: any = {
		payment_date: moment().format('YYYY-MM-DD'),
		is_confirmed: 0
	};
	inquiryPayments: any = [];
	paymentminDate: any = '';

	banks: any = [];

	// Notes
	inquiryNote: any = {};
	isLoadingNotes: boolean = false;
	inquiryNotes: any = [];
	isDeleteNote: boolean = false;
	isLoadingInvoices: boolean = false;
	bookingInvoices: any = [];

	// Attachment
	afuConfig: any = {};
	uploadInvoiceConfig: any = {};

	userPermissions: any = {};
	revertSalePermission: boolean = false;

	constructor(public dialogRef: MatDialogRef<AddInquiryComponent>,
				@Inject(MAT_DIALOG_DATA) public data: any,
				private cdr: ChangeDetectorRef,
				private layoutUtilsService: LayoutUtilsService,
				private toastService: ToastrService,
				private companyService: CompanyService,
				private commonService: CommonService,
				private http: HttpClient,
				private router: Router,
				private permissions: NgxPermissionsService,
				private modalService: NgbModal,
				private tokenStorage: TokenStorage,
				private  inquiryService: InquiryService) {

		apiURL = environment.API_URL + '/companies/';
		companySearchParams = {
			pageSize: '10',
			q: '',
			type: '0',
			_sort: 'name'
		};

		this.loggedInUser = this.tokenStorage.getUserDetail();
		const isGranted = this.loggedInUser.permissions.indexOf('action-to-confirm-payment');
		if (isGranted >= 0) {
			this.isPaymentConfirmRight = true;
		}

		const allowRemoveInvoice = this.loggedInUser.permissions.indexOf('remove-booking-invoice');
		if (allowRemoveInvoice >= 0) {
			this.allowRemoveInvoice = true;
		}

		this.initAttachmentConfig();
		if (this.data) {
			if (data.detailViewType) {
				this.detailViewType = data.detailViewType;
			}
			this.sources = this.data.sources;
			this.assignToList = this.data.assignToList;
			this.subStatuses = this.data.subStatuses;
			this.types = this.data.types;
			this.hotels = this.data.hotels;
			this.originalInquiry = this.data.originalBooking;
			this.newInquiry = this.data.currentRecord;
			this.travelAgents = this.data.travel_agents;
			this.companies = this.data.companies;
			this.additionalParams = this.data.params;
			if (this.additionalParams.payment) {
				this.tabClick({index: 2});
			} else if (this.additionalParams.selectTab !== undefined) {
				this.detailViewType = 0;
				this.tabClick({index: this.additionalParams.selectTab});
			}

			if (this.data.statuses) {
				this.statuses = this.data.statuses.filter(item => {
					if (((!item.is_waitlisted && !item.is_confirmed && !item.is_sold) ||
						(this.newInquiry.id && this.newInquiry.status_id === item.id))) {
						return true;
					}
					return false;
				});
			}
		}

		this.maxDate = moment();
		this.checkInMinDate = this.maxDate;
		this.checkOutMinDate = this.checkInMinDate;

		const yearStart: any = this.tokenStorage.getAppConfigByKey('financial_year_start_date');
		if (yearStart) {
			this.minInqDate = moment(yearStart, 'YYYY-MM-DD');
		}

		if (!this.newInquiry.id) {
			this.newInquiry = new InquiryModel();
			this.newInquiry.clear();
			this.newInquiry.inquiry_date = moment().format('YYYY-MM-DD');
			this.newInquiry.adults = 1;
			this.newInquiry.rooms = 1;
			this.newInquiry.sub_status_id = null;
			this.newInquiry.assign_to_id = this.loggedInUser.id;
			this.sourceChange(this.newInquiry.company_id);

			const pastDaysToAdd: any = this.tokenStorage.getAppConfigByKey('allow_add_past_inq_for_days');
			if (pastDaysToAdd) {
				this.minInqDate = moment().subtract(pastDaysToAdd, 'days');
			}
		} else {
			this.sourceChange(this.newInquiry.company_id);
			this.travel_agent.setValue(this.newInquiry.company_id);
			this.companyList.setValue(this.newInquiry.company_id);
			this.ota.setValue(this.newInquiry.company_id);
			this.checkInMinDate = this.newInquiry.inquiry_date;
			this.checkOutMinDate = this.checkInMinDate;
			this.loadInquiryHistory();
			this.setCurrentStatus();
			this.setCurrentSource();
			this.originalInquiry.status = this.currentStatus;
			this.paymentminDate = this.newInquiry.inquiry_date;
			this.setCurrentInqType();
		}

		this.initCompanyDataSource();

		this.userPermissions = this.permissions.getPermissions();
		if (this.userPermissions['revert-sale']) {
			this.revertSalePermission = true;
		}
	}

	ngOnInit() {
		this.getPaymentModes();
		this.getAllHotelBanks();
		this.initInquiryNote();
	}

	initPayment() {
		this.paymentEntry = {
			payment_date: moment().format('YYYY-MM-DD'),
			outstanding_amount: 0
		};
		this.reject_note = '';
		this.viewPaymentForm = false;
		this.setCurrentPaymentMode();
		this.getPayments();
	}

	initInquiryNote() {
		this.inquiryNote = {
			note: '',
			inquiry_id: this.newInquiry.id
		};
		if (this.newInquiry.id) {
			this.getNotes();
		}
	}

	closeDialog(result: any = '') {
		this.dialogRef.close(result);
	}

	sourceValueChanged() {
		if (this.companyList.value) {
			const compDetail = this.companyListArray.find(item => item.id === this.companyList.value);
			if (compDetail) {
				if (!this.newInquiry.email && compDetail.email) {
					this.newInquiry.email = compDetail.email;
				}
				if (!this.newInquiry.phone && compDetail.mobile) {
					this.newInquiry.phone = compDetail.mobile;
				}
			}
		}
	}

	sourceChange(companyId: any = null) {
		const source = this.sources.find(item => item.id === this.newInquiry.source_id);
		this.isVisibleCompanies = false;
		this.isVisibleAgents = false;
		this.isVisibleOTA = false;
		if (source) {
			if (source.name === 'Travel Agent') {
				this.isVisibleAgents = true;
				companySearchParams.type = 1;
				this.initCompanyDataSource();
				this.companyList.setValue(companyId);
			} else if (source.name === 'Company') {
				this.isVisibleCompanies = true;
				companySearchParams.type = 0;
				this.initCompanyDataSource();
				this.companyList.setValue(companyId);
			} else if (source.name === 'OTA') {
				this.isVisibleOTA = true;
				companySearchParams.type = 2;
				this.initCompanyDataSource();
				this.companyList.setValue(companyId);
			} else {
				this.newInquiry.travel_agent_id = null;
				this.newInquiry.company_id = null;
				this.companyList.setValue(null);
			}
		} else {
			this.newInquiry.travel_agent_id = null;
			this.newInquiry.company_id = null;
		}
		this.setCurrentSource();
	}

	saveInquiry() {
		this.spinner.active = true;
		this.isSaving = true;
		this.newInquiry.travel_agent_id = this.travel_agent.value;
		this.newInquiry.company_id = this.companyList.value;

		if (this.isVisibleCompanies || this.isVisibleOTA || this.isVisibleAgents) {
			this.newInquiry.company_id = this.companyList.value;
		}

		this.newInquiry.detailViewType = this.detailViewType;
		this.inquiryService.createInquiry(this.newInquiry)
			.subscribe(response => {
					this.isSaving = false;
					this.spinner.active = false;
					this.cdr.detectChanges();
					this.toastService.success('Saved Successfully', 'Success');

					if (this.currentStatus.is_lost === true) {
						this.checkWaitListedAvailability();
					}

					this.closeDialog(response);
				},
				error => {
					this.isSaving = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					this.spinner.active = false;
					this.cdr.detectChanges();
				});
	}

	checkWaitListedAvailability() {
		let reqParams;

		let waitListedStatusIds;
		waitListedStatusIds = [];
		this.data.statuses.forEach(itm => {
			if (itm.is_waitlisted === true) {
				waitListedStatusIds.push(itm.id);
			}
		});

		reqParams = {
			hotel_id: this.newInquiry.hotel_id,
			check_in_date_fromDate: this.newInquiry.check_in_date,
			check_in_date_toDate: this.newInquiry.check_out_date,
			check_out_date_fromDate: this.newInquiry.check_in_date,
			check_out_date_toDate: this.newInquiry.check_out_date,
			room_types: [],
			status_id: waitListedStatusIds,
		};
		this.inquiryService.showWaitListedInquiries(reqParams);
	}

	dateChangeEvent(type: string) {
		switch (type) {
			case 'check_in_date':
				this.checkOutMinDate = this.newInquiry[type];
				break;
			case 'inquiry_date':
				this.checkInMinDate = this.newInquiry[type];
				break;
		}
		this.newInquiry[type] = this.newInquiry[type].format('YYYY-MM-DD');

		if (this.newInquiry['check_in_date'] && this.newInquiry['check_out_date']) {
			const chIDate = moment(this.newInquiry['check_in_date'], 'YYYY-MM-DD').startOf('day');
			this.checkOutMinDate = moment(this.newInquiry['check_in_date'], 'YYYY-MM-DD').startOf('day').add(1, 'days');
			const chODate = moment(this.newInquiry['check_out_date'], 'YYYY-MM-DD').endOf('day');
			this.newInquiry.room_nights = chODate.diff(chIDate, 'days');
		}
	}

	changeInquiryStatus(event) {
		console.log(event);
		if (this.originalInquiry.status && this.originalInquiry.status.is_sold === true && !this.revertSalePermission) {
			this.newInquiry.status_id = this.originalInquiry.status_id;
			this.toastService.warning('You don\'t have permission to update status', 'Please contact admin');
			return false;
		}
		this.setCurrentStatus();
	}

	setCurrentStatus() {
		this.currentStatus = this.statuses.find(item => item.id === this.newInquiry.status_id);
	}

	setCurrentSource() {
		this.currentSource = this.sources.find(item => item.id === this.newInquiry.source_id);
		if (this.currentSource && this.currentSource.name !== 'Referral') {
			this.newInquiry.referral = '';
		}
	}

	setCurrentInqType() {
		this.currentInqType = this.types.find(item => item.id === this.newInquiry.type_id);
	}

	setCurrentPaymentMode() {
		this.currentPaymentMode = this.paymentModes.find(item => item.id === this.paymentEntry.payment_mode_id);
		if (!this.currentPaymentMode) {
			this.currentPaymentMode = {};
		}
	}

	setCurrentBank() {
		this.currentBank = this.paymentModes.find(item => item.id === this.paymentEntry.payment_mode_id);
		if (!this.currentBank) {
			this.currentBank = {};
		}
	}

	getAllHotelBanks() {
		this.isLoadingPayments = true;
		this.inquiryService.getAllHotelBanks()
			.subscribe(response => {
					this.banks = response;
					this.isLoadingPayments = false;
				},
				error => {
					this.isLoadingPayments = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	filterSubStatus() {
		let result: any;
		result = [];
		this.subStatuses.forEach(stat => {
			if (stat.status_id === this.newInquiry.status_id) {
				// } || stat.status_id === null) {
				result.push(stat);
			}
		});

		return result;
	}

	loadInquiryHistory() {
		this.inquiryService.getHistory(this.newInquiry.id)
			.subscribe(response => {
					this.histories = response;
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	openHistory(content) {
		this.modalService.open(content).result.then((result) => {
			// this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			// this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}

	sendDetailMail(id) {
		this.sendingEmail = true;
		this.inquiryService.sendDetailMail(id, {type: 'updated_inquiry_detail'})
			.subscribe(response => {
					this.sendingEmail = false;
					this.toastService.success('Mail Send Success', 'Success');
				},
				error => {
					this.sendingEmail = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	getPaymentModes() {
		this.commonService.getPaymentModes()
			.subscribe(response => {
					this.paymentModes = response;
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}

	savePayment() {
		let totalEarned = (this.paymentEntry.amount) ? parseFloat(this.paymentEntry.amount) : 0;
		this.inquiryPayments.forEach(trn => {
			if (this.paymentEntry.id !== trn.id) {
				totalEarned += trn.amount;
			}
		});
		if (totalEarned > this.newInquiry.offer_rate) {
			this.toastService.error('Amount can not be greater then inquiry amount');
			return;
		}

		this.paymentEntry.inquiry_id = this.newInquiry.id;
		this.isSaving = true;
		this.inquiryService.saveInquiryPayment(this.paymentEntry)
			.subscribe(response => {
					this.isSaving = false;
					this.toastService.success('Saved Successfully', 'Success');
					this.initPayment();
				},
				error => {
					this.isSaving = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	requestToReconfirm() {
		this.paymentEntry.is_rejected = false;
		this.savePayment();
	}

	getPayments() {
		this.isLoadingPayments = true;
		this.inquiryService.getInquiryPayment(this.newInquiry.id)
			.subscribe(response => {
					this.isLoadingPayments = false;
					if (response) {
						this.paymentEntry.outstanding_amount = response.outstandingAmount;
						this.inquiryPayments = response.payments;
						if (this.additionalParams.payment) {
							const payRecObject = this.inquiryPayments.find(item => item.id === parseInt(this.additionalParams.payment));
							if (payRecObject) {
								this.editPayment(payRecObject);
							}
						}
					}
				},
				error => {
					this.isLoadingPayments = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	editPayment(entry) {
		entry.isReadOnly = false;
		if (entry.is_confirmed === true && !this.isPaymentConfirmRight) {
			entry.isReadOnly = true;
		}
		this.paymentEntry = Object.assign({outstanding_amount: this.paymentEntry.outstanding_amount}, entry);
		this.setCurrentPaymentMode();
		this.initAttachmentConfig(this.paymentEntry.id);
		this.viewPaymentForm = true;
	}

	sendPaymentRequest(record) {
		this.sendingPaymentRequest = true;
		this.inquiryService.sendPaymentRequest(record.id)
			.subscribe(response => {
					this.toastService.success('Payment Request send Successfully', 'Done');
					this.sendingPaymentRequest = false;
				},
				error => {
					this.sendingPaymentRequest = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	paymentDateChangeEvent(type: string) {
		this.paymentEntry[type] = this.paymentEntry[type].format('YYYY-MM-DD HH:mm:ss');
	}

	deletePayment(record) {
		const dialogRef = this.layoutUtilsService.deleteElement();
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.isDeletePayment = true;
			this.inquiryService.deleteInquiryPayment(record.id)
				.subscribe(response => {
						this.isDeletePayment = false;
						this.getPayments();
						this.toastService.success('Deleted Successfully', 'Done');
					},
					error => {
						this.isDeletePayment = false;
						this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					});
		});
	}

	saveNote() {
		this.inquiryNote.inquiry_id = this.newInquiry.id;
		this.isSaving = true;
		this.inquiryService.saveInquiryNote(this.inquiryNote)
			.subscribe(response => {
					this.isSaving = false;
					this.toastService.success('Saved Successfully', 'Success');
					this.initInquiryNote();
				},
				error => {
					this.isSaving = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	getNotes() {
		this.isLoadingNotes = true;
		this.inquiryService.getInquiryNotes(this.newInquiry.id)
			.subscribe(response => {
					this.isLoadingNotes = false;
					if (response) {
						this.inquiryNotes = response;
					}
				},
				error => {
					this.isLoadingNotes = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	deleteNote(record) {
		const dialogRef = this.layoutUtilsService.deleteElement();
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.isDeleteNote = true;
			this.inquiryService.deleteInquiryNote(record.inquiry_id, record.id)
				.subscribe(response => {
						this.isDeleteNote = false;
						this.getNotes();
						this.toastService.success('Deleted Successfully', 'Done');
					},
					error => {
						this.isDeleteNote = false;
						this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					});
		});
	}

	backToDetailTab() {
		this.selectedTabIndex = 0;
	}

	goBack() {
		this.selectedTabIndex -= 1;
	}

	tabClick(event) {
		this.selectedTabIndex = event.index;
		switch (this.selectedTabIndex) {
			case 0:
				this.initInquiryNote();
				break;
			case 1:
				this.initPayment();
				break;
			case 2:
			case 3:
				this.initPayment();
				this.initInvoiceUploadConfig();
				this.getBookingInvoices();
				break;
		}
	}

	initAttachmentConfig(payment_id: any = '') {
		const accessToken = localStorage.getItem('accessToken');
		this.afuConfig = {
			multiple: false,
			formatsAllowed: '.jpg,.png,.pdf,.docx, .txt,.gif,.jpeg,.xls,.xlsx',
			maxSize: '2',
			uploadAPI: {
				url: this.inquiryService.getAttachmentUploadUrl(payment_id),
				headers: {
					'Authorization': 'Bearer ' + accessToken
				}
			},
			theme: 'attachPin',
			hideProgressBar: true,
			hideResetBtn: true,
			hideSelectBtn: true,
			attachPinText: ' '
		};
	}

	paymentFileUploaded(response) {
		if (response.status === 200) {
			const paymentRec = JSON.parse(response.response);
			this.paymentEntry.attach_url = paymentRec.attach_url;
			this.paymentEntry.attach_name = paymentRec.attach_name;
			this.paymentEntry.attach_path = paymentRec.attach_path;
			this.paymentEntry.attach_type = paymentRec.attach_type;
			this.paymentEntry.attach_size = paymentRec.attach_size;
			const payRecObject = this.inquiryPayments.find(item => item.id === paymentRec.id);
			if (payRecObject) {
				payRecObject.attach_url = paymentRec.attach_url;
				payRecObject.attach_name = paymentRec.attach_name;
				payRecObject.attach_path = paymentRec.attach_path;
				payRecObject.attach_type = paymentRec.attach_type;
				payRecObject.attach_size = paymentRec.attach_size;
			}
		}
	}

	removePaymentAttachment(paymentEntry) {
		const dialogRef = this.layoutUtilsService.deleteElement();
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.inquiryService.deletePaymentAttachment(paymentEntry.inquiry_id, paymentEntry.id)
				.subscribe(response => {
						this.toastService.success('Deleted Successfully', 'Done');
						this.paymentEntry.attach_url = response.attach_url;
						this.paymentEntry.attach_name = response.attach_name;
						this.paymentEntry.attach_path = response.attach_path;
						this.paymentEntry.attach_type = response.attach_type;
						this.paymentEntry.attach_size = response.attach_size;
						const payRecObject = this.inquiryPayments.find(item => item.id === paymentEntry.id);
						if (payRecObject) {
							payRecObject.attach_url = response.attach_url;
							payRecObject.attach_name = response.attach_name;
							payRecObject.attach_path = response.attach_path;
							payRecObject.attach_type = response.attach_type;
							payRecObject.attach_size = response.attach_size;
						}

					},
					error => {
						this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					});
		});
	}

	rejectPayment() {
		this.isSaving = true;
		this.inquiryService.rejectPayment(this.paymentEntry.inquiry_id, {
			confirm_note: this.reject_note,
			id: this.paymentEntry.id
		})
			.subscribe(response => {
					this.isSaving = false;
					this.toastService.success('Payment rejected', 'Success');
					this.initPayment();
				},
				error => {
					this.isSaving = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					this.cdr.detectChanges();
				});
	}

	initCompanyDataSource() {
		const http = this.http;
		let me: any;
		me = this;
		this.companyDataSource = {
			displayValue(value: any): Observable<any | null> {
				if (typeof value === 'string') {
					value = parseInt(value, 10);
				}
				if (typeof value !== 'number') {
					return of(null);
				}

				return http.get<any>(apiURL + 'find/' + value).pipe(
					map(e => ({
						value: e.id,
						display: `${e.name}`,
						details: {}
					}))
				);
			},
			search(term: string): Observable<any[]> {
				companySearchParams.q = term || '';
				return http.get<any[]>(apiURL + 'search', {
					params: companySearchParams
				}).pipe(
					map(list => {
						me.companyListArray = list;
						return list.map(e => {
							return {
								value: e.id,
								display: `${e.name}`,
								details: {}
							};
						});
					}));
			}
		};
	}

	initInvoiceUploadConfig() {
		const accessToken = localStorage.getItem('accessToken');
		this.uploadInvoiceConfig = {
			multiple: false,
			formatsAllowed: '.jpg,.png,.pdf,.jpeg',
			maxSize: '2',
			uploadAPI: {
				url: this.inquiryService.getAttachmentInvoiceUrl(this.newInquiry.id),
				headers: {
					'Authorization': 'Bearer ' + accessToken
				}
			},
			theme: 'attachPin',
			hideProgressBar: true,
			hideResetBtn: true,
			hideSelectBtn: true,
			attachPinText: 'Upload Invoice'
		};
	}

	getBookingInvoices() {
		this.isLoadingInvoices = true;
		this.inquiryService.getInvoices(this.newInquiry.id)
			.subscribe(response => {
					this.isLoadingInvoices = false;
					if (response) {
						this.bookingInvoices = response;
					}
				},
				error => {
					this.isLoadingInvoices = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	isCheckedOut() {
		const chOut = moment(this.newInquiry['check_out_date'], 'YYYY-MM-DD');
		const today = moment();
		if (chOut <= today && this.currentStatus.is_sold) {
			return true;
		}
		return false;
	}

	getFileName(fullPath) {
		return fullPath.replace(/^.*[\\\/]/, '');
	}

	uploadInvoice(response) {
		if (response.status === 200) {
			const invoiceResp = JSON.parse(response.response);
			this.newInquiry.invoice_url = invoiceResp.invoice_url;
			this.toastService.success('Invoice Upload Successfully', 'Success');
			this.initInvoiceUploadConfig();
			this.getBookingInvoices();
		}
	}

	removeBookingInvoice(invoice) {
		const dialogRef = this.layoutUtilsService.deleteElement('Delete Confirm', 'Are you sure to delete this ?');
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.inquiryService.deleteBookingInvoice(this.newInquiry.id, invoice.id)
				.subscribe(response => {
						this.toastService.success('Deleted Successfully', 'Done');
						this.getBookingInvoices();
					},
					error => {
						this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					});
		});
	}
}
