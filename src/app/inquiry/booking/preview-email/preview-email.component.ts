import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'm-preview-email',
    templateUrl: './preview-email.component.html',
    styleUrls: ['./preview-email.component.scss']
})
export class PreviewEmailComponent implements OnInit {
    isSaving: boolean = false;
    currentRecord: any = {};

    constructor(private dialogRef: MatDialogRef<PreviewEmailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        if (this.data && this.data.record) {
                this.currentRecord = this.data.record;
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }
}
