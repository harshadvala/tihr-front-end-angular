import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {CompanyService} from '../../services/company.service';
import {CommonService} from '../../services/common.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {InquiryService} from '../inquiry.service';

@Component({
    selector: 'm-inq-payment-confirm',
    templateUrl: './inq-payment-confirm.component.html',
    styleUrls: ['./inq-payment-confirm.component.scss'],
    providers: [InquiryService, CompanyService, CommonService]
})
export class InqPaymentConfirmComponent implements OnInit {

    isSaving: boolean = false;
    paymentEntry: any = {};
    paymentRecord: any = {};

    constructor(public dialogRef: MatDialogRef<InqPaymentConfirmComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef,
                private toastService: ToastrService,
                private commonService: CommonService,
                private modalService: NgbModal,
                private tokenStorage: TokenStorage,
                private  inquiryService: InquiryService) {
        if (this.data) {
            this.paymentRecord = data.paymentDetail;
            this.paymentEntry.id = this.paymentRecord.id;
            this.paymentEntry.iq_id = this.paymentRecord.iq_id;
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    confirmPayment() {
        this.isSaving = true;
        this.inquiryService.confirmPayment(this.paymentEntry.iq_id, this.paymentEntry)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Payment confirmed', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.cdr.detectChanges();
                });
    }

    rejectPayment() {
        this.isSaving = true;
        this.inquiryService.rejectPayment(this.paymentEntry.iq_id, this.paymentEntry)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Payment rejected', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.cdr.detectChanges();
                });
    }

}
