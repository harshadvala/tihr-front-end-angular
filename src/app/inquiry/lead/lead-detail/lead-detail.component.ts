import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {TokenStorage} from '../../../core/auth/token-storage.service';
import {InquiryService} from '../../inquiry.service';

@Component({
    selector: 'm-lead-detail',
    templateUrl: './lead-detail.component.html',
    styleUrls: ['./lead-detail.component.scss'],
    providers: [InquiryService]
})
export class LeadDetailComponent implements OnInit {
    currentRecord: any = {};
    cityLists: any = [];
    isLoading: boolean = false;
    isSaving: boolean = false;

    constructor(public dialogRef: MatDialogRef<LeadDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef,
                private toastService: ToastrService,
                private  inquiryService: InquiryService,
                private tokenStorage: TokenStorage) {
        if (this.data) {
            this.currentRecord = data.leadDetail;
            this.cityLists = data.cityLists;
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.inquiryService.saveLead(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.currentRecord = {};
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }
}
