import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {InquiryService} from '../inquiry.service';
import {UserService} from '../../services/user.service';
import {HotelService} from '../../services/hotel.service';
import {InquiryDataSource} from '../../Models/data-sources/inquiry.dataSource';
import {SpinnerButtonOptions} from '../../content/partials/content/general/spinner-button/button-options.interface';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {FormControl} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import * as moment from 'moment';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {fromEvent, merge} from 'rxjs';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {CommonService} from '../../services/common.service';
import {LeadDetailComponent} from './lead-detail/lead-detail.component';

@Component({
    selector: 'm-lead',
    templateUrl: './lead.component.html',
    styleUrls: ['./lead.component.scss'],
    providers: [InquiryService, UserService, HotelService, CommonService]
})
export class LeadComponent implements OnInit {
    loggedInUser: any = {};
    currentRecord: any = {};
    inquiryFilters: any = {};
    dataSource: any = InquiryDataSource;
    leads: any[] = [];
    isLoading: boolean = false;
    isSaving: boolean = false;
    itemsPerPage: number = 10;
    entryModalRef: any;
    cityLists: any[] = [];
    spinner: SpinnerButtonOptions = {
        active: false,
        spinnerSize: 18,
        raised: true,
        buttonColor: 'primary',
        spinnerColor: 'accent',
        fullWidth: false
    };

    displayedColumns: string[] = [
        'select',
        'name',
        'email',
        'mobile',
        'cities',
        'created_at',
        'actions'
    ];

    // displayedColumns = ['id', 'name','inquiry_date'];
    // dataSource = new MatTableDataSource(this.inquiries);
    selection = new SelectionModel<any>(true, []);

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    isVisibleCompanies: any = false;
    isVisibleAgents: any = false;

    travel_agent = new FormControl(null, []);
    travelAgentDataSource: DataSource;

    companyList = new FormControl(null, []);
    companyDataSource: DataSource;

    dateOptions: any = {
        locale: {format: 'DD-MM-YYYY'},
        alwaysShowCalendars: false,
        startDate: moment(),
        endDate: moment(),
        opens: 'center',
    };

    constructor(private modalService: NgbModal,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private http: HttpClient,
                private _route: ActivatedRoute,
                private commonService: CommonService,
                private  inquiryService: InquiryService,
                private hotelService: HotelService,
                private toastService: ToastrService,
                private activatedRoute: ActivatedRoute,
                private layoutUtilsService: LayoutUtilsService,
                private tokenStorage: TokenStorage,
                private  userService: UserService) {

        this.loggedInUser = this.tokenStorage.getUserDetail();
    }

    ngOnInit() {
        this.loadCities();
        this.paginator.page
            .pipe(
                tap(() => this.loadLeadListRequest())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadLeadListRequest();
                })
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadLeadListRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'desc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new InquiryDataSource(this.inquiryService);
        // First load
        this.dataSource.loadLeads(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.leads = res));
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    loadLeadListRequest() {
        const q = this.searchInput.nativeElement.value;
        let queryParams: any = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);

        queryParams = Object.assign(queryParams, this.inquiryFilters);

        this.dataSource.loadLeads(queryParams);
        this.selection.clear();
    }

    deleteLead(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            record.isDeleting = true;
            this.inquiryService.deleteLead(record.id)
                .subscribe(response => {
                        this.toastService.success('Deleted Successfully', 'Success');
                        record.isDeleting = false;
                        this.refreshCurrentPage();
                    },
                    error => {
                        record.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });

        });
    }

    loadCities() {
        this.commonService.searchCities({id: '632,633,634,635,636,489,474,475,479'})
            .subscribe(response => {
                    this.cityLists = response;
                    console.log(this.cityLists);
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.leads.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.leads.length) {
            this.selection.clear();
        } else {
            this.leads.forEach(row => this.selection.select(row));
        }
    }

    openEntryForm(content, record: any = {}) {
        if (record) {
            this.currentRecord = record;
        } else {
            this.currentRecord = {};
        }
        this.entryModalRef = this.modalService.open(content, {centered: true});
    }

    save() {
        this.isSaving = true;
        this.inquiryService.saveLead(this.currentRecord)
            .subscribe(response => {
                    this.entryModalRef.close();
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.currentRecord = {};
                    this.refreshCurrentPage();
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    refreshCurrentPage() {
        this.paginator.pageIndex -= 1;
        this.loadLeadListRequest();
    }

    openDetailForm(currentRecord: any = {}, params: any = {}) {
        const dialogRef = this.dialog.open(LeadDetailComponent, {
            width: '500px',
            data: {
                cityLists: this.cityLists,
                leadDetail: Object.assign({}, currentRecord),
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.refreshCurrentPage();
            }
        });
    }
}
