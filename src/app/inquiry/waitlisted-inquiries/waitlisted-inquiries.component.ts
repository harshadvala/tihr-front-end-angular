import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'm-waitlisted-inquiries',
    templateUrl: './waitlisted-inquiries.component.html',
    styleUrls: ['./waitlisted-inquiries.component.scss']
})
export class WaitlistedInquiriesComponent implements OnInit {
    isLoading: boolean = false;
    waitListedInquiries: any[] = [];

    constructor(public dialogRef: MatDialogRef<WaitlistedInquiriesComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef) {
        if (this.data) {
            this.waitListedInquiries = data.inquiries;
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }
}
