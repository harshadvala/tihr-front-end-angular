import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {InquiryService} from '../../inquiry/inquiry.service';
import {UserService} from '../../services/user.service';
import {HotelService} from '../../services/hotel.service';
import {SpinnerButtonOptions} from '../../content/partials/content/general/spinner-button/button-options.interface';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {FormControl} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {environment} from '../../../environments/environment';
import {fromEvent, Observable, of} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, tap} from 'rxjs/operators';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {ReportService} from '../../services/report.service';
import * as moment from 'moment';
import {DaterangePickerComponent} from 'ng2-daterangepicker';
import {UtilsService} from '../../core/services/utils.service';

@Component({
    selector: 'm-booking-discount-report',
    templateUrl: './booking-discount-report.component.html',
    styleUrls: ['./booking-discount-report.component.scss'],
    providers: [InquiryService, UserService, HotelService, ReportService, UtilsService]
})
export class BookingDiscountReportComponent implements OnInit {
    isCollapsedFilter: boolean = false;
    dateOptions: any = {};
    customFilters: any = {
        ageing_filter: {
            operator: '>',
            range_from: 0,
            range_to: 10
        },
        pending_filter: {
            operator: '',
            range_from: 0,
            range_to: 100
        },
        disc_percent_filter: {
            operator: '>',
            range_from: 0,
            range_to: 10
        },
        discount_filter: {
            operator: '',
            range_from: 0,
            range_to: 100
        },
        source_id: [],
        group_by: '',
    };
    reportSummary = {totalDiscount: 0, totalAMount: 0, totalPaid: 0, totalPending: 0};
    inquirySourceList: any = [];
    inquiryStatusList: any = [];
    selectedStatuses = [];
    statusSettings: any = {};
    inquirySubStatusList: any = [];
    inquiryTypesList: any = [];
    reportRecordLists: any = [];
    reportRecordGroup: any = {};
    reportCollectionCount: number = 0;
    isLoading: boolean = false;
    itemsPerPage: number = 20;
    page: number = 1;
    assignToUserList: any = [];
    travelAgents: any = [];
    companies: any = [];
    baseUrl: any = environment.Base_Url;
    spinner: SpinnerButtonOptions = {
        active: false,
        spinnerSize: 18,
        raised: true,
        buttonColor: 'primary',
        spinnerColor: 'accent',
        fullWidth: false
    };

    hotelList: any = [];
    hotelListSettings: any = {};
    selectedHotelLists = [];
    userList: any = [];
    userListSettings: any = {};
    selectedUserLists = [];
    selectedSourcesLists = [];
    sourceSettings = {
        singleSelection: false,
        text: 'Select Source',
        selectAllText: 'All',
        unSelectAllText: 'All',
        enableSearchFilter: false,
        classes: 'custom-dropdown-list',
        labelKey: 'name',
        badgeShowLimit: 2,
        primaryKey: 'id',
        maxHeight: 250
    };

    companySettings: any = {
        singleSelection: false,
        text: 'Select Company / OTA / TA',
        selectAllText: 'All',
        unSelectAllText: 'All',
        enableSearchFilter: true,
        classes: 'custom-dropdown-list',
        labelKey: 'name',
        badgeShowLimit: 2,
        primaryKey: 'id',
        maxHeight: 250
    };

    displayedColumns: string[] = [
        'hotel',
        'assign_to',
        'inquiry_id',
        'booking_id',
        'name',
        'inquiry_sub_status',
        'inquiry_date',
        'check_in_date',
        'check_out_date',
        'offer_rate',
        'paid_amount',
        'outstanding_amount',
        'source',
        'travel_agent'
    ];

    selection = new SelectionModel<any>(true, []);

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;
    @ViewChild('checkInDateInput') checkInDateInput: DaterangePickerComponent;


    isVisibleCompanies: any = false;
    isVisibleAgents: any = false;
    agingOperator: any = false;
    agingFrom: number;
    agingTo: number;

    travel_agent = new FormControl(null, []);
    travelAgentDataSource: DataSource;

    companyList = new FormControl(null, []);
    companyDataSource: DataSource;
    companyLists: any = [];
    selectedCompanies: any = [];
    searchCompanyTypes: any = [];

    constructor(private modalService: NgbModal,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private http: HttpClient,
                private inquiryService: InquiryService,
                private reportService: ReportService,
                private hotelService: HotelService,
                private toastService: ToastrService,
                private layoutUtilsService: LayoutUtilsService,
                private utilsService: UtilsService,
                private  userService: UserService) {

        this.dateOptions = this.utilsService.getCalenderConfig({
            startDate: moment(),
            endDate: moment(),
        });

        this.statusSettings = this.utilsService.getMultiSelctConfig({text: 'Select Status', badgeShowLimit: 2});
        this.hotelListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Hotels', groupBy: 'category', labelKey: 'code'});

        const apiURL = environment.API_URL + '/companies/';

        this.userListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Assign To', badgeShowLimit: 1, groupBy: 'category'});

        this.travelAgentDataSource = {
            displayValue(value: any): Observable<any | null> {
                console.log('finding display value for', value);
                if (typeof value === 'string') {
                    value = parseInt(value, 10);
                }
                if (typeof value !== 'number') {
                    return of(null);
                }

                return http.get<any>(apiURL + 'find/' + value).pipe(
                    map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))
                );
            },
            search(term: string): Observable<any[]> {
                return http.get<any[]>(apiURL + 'search', {
                    params: {
                        pageSize: '5',
                        q: term || '',
                        type: '1',
                        _sort: 'name'
                    }
                }).pipe(
                    map(list => list.map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))));
            }
        };


        this.companyDataSource = {
            displayValue(value: any): Observable<any | null> {
                console.log('finding display value for', value);
                if (typeof value === 'string') {
                    value = parseInt(value, 10);
                }
                if (typeof value !== 'number') {
                    return of(null);
                }

                return http.get<any>(apiURL + 'find/' + value).pipe(
                    map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))
                );
            },
            search(term: string): Observable<any[]> {
                return http.get<any[]>(apiURL + 'search', {
                    params: {
                        pageSize: '5',
                        q: term || '',
                        type: '0',
                        _sort: 'name'
                    }
                }).pipe(
                    map(list => list.map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))));
            }
        };
    }

    ngOnInit() {
        this.getSettingLists();
        this.initFilters();

        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.page = 1;
                    this.leadReportDateRequest();
                })
            )
            .subscribe();
    }

    initFilters() {
        this.customFilters = {
            check_in_date_fromDate: moment().format('YYYY-MM-DD'),
            check_in_date_toDate: moment().format('YYYY-MM-DD'),
            check_out_date_fromDate: moment().format('YYYY-MM-DD'),
            check_out_date_toDate: moment().format('YYYY-MM-DD'),
            inquiry_fromDate: moment().format('YYYY-MM-DD'),
            inquiry_toDate: moment().format('YYYY-MM-DD'),
            check_in_date_filter: false,
            check_out_date_filter: false,
            inquiry_date_filter: false,
            ageing_filter: {
                operator: '>',
                range_from: 0,
                range_to: 10
            },
            pending_filter: {
                operator: '>',
                range_from: 0,
                range_to: 0
            },
            disc_percent_filter: {
                operator: '>',
                range_from: 0,
                range_to: 10
            },
            discount_filter: {
                operator: '',
                range_from: 0,
                range_to: 0
            },
            sortField: 'inquiry_id',
            sortOrder: 'asc',
            source_id: [],
            group_by: '',
        };
    }

    leadReportDateRequest() {
        this.reportSummary = {totalDiscount: 0, totalAMount: 0, totalPaid: 0, totalPending: 0};
        const queryParams = this.prepareFilterParameters();
        this.isLoading = true;
        this.reportService.bookingOutStanding(queryParams)
            .subscribe(response => {
                    this.isLoading = false;
                    if (response.items) {
                        if (this.customFilters.group_by) {
                            this.reportRecordLists = [];
                            this.reportRecordGroup = response.items;
                        } else {
                            this.reportRecordLists = response.items;
                            this.reportRecordGroup = {};
                        }
                        this.reportCollectionCount = response.totalCount;
                        this.reportSummary.totalDiscount = response.totalDiscount;
                        this.reportSummary.totalAMount = response.totalAmount;
                        this.reportSummary.totalPaid = response.totalPaid;
                        this.reportSummary.totalPending = response.totalPending;
                    }
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getSum(data, column): number { // console.log(data);
        let sum = 0;
        for (let i = 0; i < data.length; i++) {
            if (data[i][column]) {
                sum += parseFloat(data[i][column]);
            }
        }
        return sum;
    }


    prepareFilterParameters() {
        const q = this.searchInput.nativeElement.value;
        let queryParams: any = new QueryParamsModel(
            q,
            '',
            '',
            this.page,
            this.itemsPerPage);

        queryParams = Object.assign(queryParams, this.customFilters);

        delete queryParams.check_in_date_filter;
        delete queryParams.check_out_date_filter;
        delete queryParams.inquiry_date_filter;

        if (!this.customFilters.inquiry_date_filter) {
            delete queryParams.inquiry_fromDate;
            delete queryParams.inquiry_toDate;
        }

        if (!this.customFilters.check_in_date_filter) {
            delete queryParams.check_in_date_fromDate;
            delete queryParams.check_in_date_toDate;
        }

        if (!this.customFilters.check_out_date_filter) {
            delete queryParams.check_out_date_fromDate;
            delete queryParams.check_out_date_toDate;
        }

        return queryParams;
    }

    applyFilter() {
        this.page = 1;
        // this.onUserSelect(); // this.onHotelSelect();
        if (this.isVisibleAgents) {
            this.customFilters.travel_agent_id = this.travel_agent.value;
        }
        if (this.isVisibleCompanies) {
            this.customFilters.company_id = this.companyList.value;
        }
        if (this.agingOperator) {
            this.customFilters.aging_operator = this.agingOperator;
        }
        if (this.agingOperator && this.agingFrom) {
            this.customFilters.aging_from = this.agingFrom;
        }
        if (this.agingOperator && this.agingOperator === 'bt' && this.agingTo) {
            this.customFilters.aging_to = this.agingTo;
        }
        this.leadReportDateRequest();
    }

    pageChange(page) {
        this.page = page;
        this.leadReportDateRequest();
    }

    resetFilter() {
        this.selectedUserLists = [];
        this.selectedHotelLists = [];
        this.selectedSourcesLists = [];
        this.companyList = new FormControl(null, []);
        this.travel_agent = new FormControl(null, []);
        this.isCollapsedFilter = true;
        this.initFilters();
        // this.paginator.pageIndex = 0;
        this.page = 1;
        this.leadReportDateRequest();
    }

    getSettingLists() {
        this.inquiryService.getInquirySettingLists()
            .subscribe(response => {
                    this.assignToUserList = response.users;
                    this.inquirySourceList = response.sources;
                    /*this.inquiryStatusList = response.statuses;*/
                    this.inquirySubStatusList = response.subStatuses;
                    // this.userList = response.users;
                    // this.hotelList = response.hotels;
                    this.inquiryTypesList = response.types;
                    this.travelAgents = response.travel_agents;
                    this.companies = response.companies;

                    this.hotelList = [];
                    response.hotels.forEach(itm => {
                        if (itm.is_enabled) {
                            itm.category = 'Active';
                            this.selectedHotelLists.push(itm);
                        } else {
                            itm.category = 'Disabled';
                        }
                        this.hotelList.push(itm);
                    });
                    this.hotelList = this.hotelList.sort((a, b) => a.category.localeCompare(b.category));
                    this.onHotelSelect();

                    this.userList = [];
                    response.users.forEach(itm => {
                        if (!!itm.status) {
                            itm.category = 'Active';
                            this.selectedUserLists.push(itm);
                        } else {
                            itm.category = 'Disabled';
                        }
                        this.userList.push(itm);
                    });
                    this.userList = this.userList.sort((a, b) => a.category.localeCompare(b.category));
                    this.onUserSelect();

                    response.statuses.forEach(itm => {
                        if (itm.is_confirmed === true || itm.is_sold === true) {
                            this.inquiryStatusList.push(itm);
                            this.selectedStatuses.push(itm);
                        }
                    });
                    this.onStatusSelect();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    sourceChange() {
        const source = this.inquirySourceList.find(item => item.id === this.customFilters.source_id);
        this.isVisibleCompanies = false;
        this.isVisibleAgents = false;
        if (source) {
            if (source.name === 'Travel Agent') {
                this.isVisibleAgents = true;
                this.customFilters.company_id = '';
            } else if (source.name === 'Company') {
                this.isVisibleCompanies = true;
                this.customFilters.travel_agent_id = '';
            } else {
                this.customFilters.travel_agent_id = '';
                this.customFilters.company_id = '';
            }
        } else {
            this.customFilters.travel_agent_id = '';
            this.customFilters.company_id = '';
        }
    }

    selectedIQDate(value: any) {
        this.customFilters.inquiry_fromDate = value.start.format('YYYY-MM-DD');
        this.customFilters.inquiry_toDate = value.end.format('YYYY-MM-DD');
    }

    clearIQDate() {
        this.customFilters.inquiry_date_filter = false;
    }

    selectedCheckInDate(value: any) {
        this.customFilters.check_in_date_fromDate = value.start.format('YYYY-MM-DD');
        this.customFilters.check_in_date_toDate = value.end.format('YYYY-MM-DD');
    }

    clearCheckInDate() {
        this.customFilters.check_in_date_filter = false;
    }

    selectedCheckOutDate(value: any) {
        this.customFilters.check_out_date_fromDate = value.start.format('YYYY-MM-DD');
        this.customFilters.check_out_date_toDate = value.end.format('YYYY-MM-DD');
    }

    clearCheckOutDate() {
        this.customFilters.check_out_date_filter = false;
    }

    exportReportData(type: string = 'pdf') {
        const queryParams = this.prepareFilterParameters();
        queryParams.type = type;
        this.isLoading = true;
        this.reportService.bookingDiscountReportExport(queryParams)
            .subscribe(response => {
                    this.isLoading = false;
                    window.open(response.message, '_blank');
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    onStatusSelect(item: any = {}) {
        this.customFilters.status_id = [];
        if (this.selectedStatuses.length > 0) {
            this.selectedStatuses.forEach(itm => {
                this.customFilters.status_id.push(itm.id);
            });
        } else {
            this.inquiryStatusList.forEach(itm => {
                this.customFilters.status_id.push(itm.id);
            });
        }
    }

    onSourceSelectionEvent(item: any = {}) {
        this.customFilters.source_id = [];
        if (this.selectedSourcesLists.length > 0) {
            this.selectedSourcesLists.forEach(itm => {
                this.customFilters.source_id.push(itm.id);
            });
        }
        this.checkCompanySourceSelected();
    }

    onUserSelect(item: any = {}) {
        this.customFilters.assign_to_id = [];
        if (this.selectedUserLists.length > 0) {
            this.selectedUserLists.forEach(itm => {
                this.customFilters.assign_to_id.push(itm.id);
            });
        }
    }

    onHotelSelect(item: any = {}) {
        console.log(this.selectedHotelLists);
        this.customFilters.hotel_id = [];
        if (this.selectedHotelLists.length > 0) {
            this.selectedHotelLists.forEach(itm => {
                this.customFilters.hotel_id.push(itm.id);
            });
        }
    }

    onCompanySelectEvent(item: any = {}) {
        this.customFilters.company_id = [];
        if (this.selectedCompanies.length > 0) {
            this.selectedCompanies.forEach(itm => {
                this.customFilters.company_id.push(itm.id);
            });
        }
    }

    checkCompanySourceSelected() {
        const company = this.inquirySourceList.find(item => item.name === 'Company');
        const ota = this.inquirySourceList.find(item => item.name === 'OTA');
        const travelAgent = this.inquirySourceList.find(item => item.name === 'Travel Agent');

        const compInd = this.customFilters.source_id.indexOf(company.id);
        const otaInd = this.customFilters.source_id.indexOf(ota.id);
        const taInd = this.customFilters.source_id.indexOf(travelAgent.id);

        this.searchCompanyTypes = [];
        if (compInd >= 0) {
            this.searchCompanyTypes.push(0);
        }

        if (taInd >= 0) {
            this.searchCompanyTypes.push(1);
        }

        if (otaInd >= 0) {
            this.searchCompanyTypes.push(2);
        }
    }

    onSearchCompany(evt: any) {
        console.log(evt.target.value);
        this.companyLists = [];
        const companySearchParams = {
            pageSize: '10',
            q: evt.target.value,
            type: this.searchCompanyTypes.join(','),
            _sort: 'name'
        };

        this.http.get(environment.API_URL + '/companies/search', {params: companySearchParams})
            .subscribe(response => {
                console.log(response);
                this.companyLists = response;
            }, error => {
            });
    }

    applySorting(sortField) {
        if (this.customFilters.sortField === sortField) {
            if (this.customFilters.sortOrder === 'asc') {
                this.customFilters.sortOrder = 'desc';
            } else {
                this.customFilters.sortOrder = 'asc';
            }
        } else {
            this.customFilters.sortOrder = 'asc';
            this.customFilters.sortField = sortField;
        }
        this.applyFilter();
    }

}
