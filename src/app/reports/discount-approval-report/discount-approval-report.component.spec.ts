import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountApprovalReportComponent } from './discount-approval-report.component';

describe('DiscountApprovalReportComponent', () => {
  let component: DiscountApprovalReportComponent;
  let fixture: ComponentFixture<DiscountApprovalReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountApprovalReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountApprovalReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
