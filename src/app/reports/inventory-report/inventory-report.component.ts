import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {InquiryService} from '../../inquiry/inquiry.service';
import {UserService} from '../../services/user.service';
import {HotelService} from '../../services/hotel.service';
import {ReportService} from '../../services/report.service';
import {BookingOutStandingDataSource} from '../../Models/data-sources/reports/booking-outstanding.dataSource';
import {environment} from '../../../environments/environment';
import {SpinnerButtonOptions} from '../../content/partials/content/general/spinner-button/button-options.interface';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {DaterangePickerComponent} from 'ng2-daterangepicker';
import {FormControl} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import * as moment from 'moment';
import {UtilsService} from '../../core/services/utils.service';

@Component({
    selector: 'm-inventory-report',
    templateUrl: './inventory-report.component.html',
    styleUrls: ['./inventory-report.component.scss'],
    providers: [InquiryService, UserService, HotelService, ReportService, UtilsService]
})
export class InventoryReportComponent implements OnInit {
    isCollapsedFilter: boolean = false;
    customFilters: any = {};
    dataSource: any = BookingOutStandingDataSource;
    inquirySourceList: any = [];
    inquiryStatusList: any = [];
    inquirySubStatusList: any = [];
    inquiryTypesList: any = [];
    hotelList: any = [];
    sortField = 'check_in_date';
    sortOrderBy = 'asc';
    isLoading: boolean = false;
    itemsPerPage: number = 10;
    assignToUserList: any = [];
    travelAgents: any = [];
    companies: any = [];
    reportData: any = [];
    dateRange = [];
    spinner: SpinnerButtonOptions = {
        active: false,
        spinnerSize: 18,
        raised: true,
        buttonColor: 'primary',
        spinnerColor: 'accent',
        fullWidth: false
    };
    hotelListSettings: any = {};
    selectedHotelLists = [];

    selection = new SelectionModel<any>(true, []);

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('checkInDateInput') checkInDateInput: DaterangePickerComponent;


    isVisibleCompanies: any = false;
    isVisibleAgents: any = false;

    travel_agent = new FormControl(null, []);
    travelAgentDataSource: DataSource;

    companyList = new FormControl(null, []);
    companyDataSource: DataSource;

    dateOptions: any = {};


    dropdownList = [];
    selectedItems = [];
    dropdownSettings1 = {};

    constructor(private modalService: NgbModal,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private http: HttpClient,
                private  inquiryService: InquiryService,
                private  reportService: ReportService,
                private hotelService: HotelService,
                private toastService: ToastrService,
                private utilsService: UtilsService,
                private layoutUtilsService: LayoutUtilsService,
                private  userService: UserService) {

        const apiURL = environment.API_URL + '/companies/';

        this.hotelListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Hotels', groupBy: 'category', labelKey: 'code'});

        this.dateOptions = this.utilsService.getCalenderConfig({
            startDate: moment(),
            endDate: moment().add(14, 'days').endOf('day'),
        });
    }

    ngOnInit() {
        this.initFilters();
        // this.getSettingLists();
        this.getHotels();
    }

    initFilters() {
        this.customFilters = {
            from_date: moment().format('YYYY-MM-DD'),
            to_date: moment().add(14, 'days').endOf('day').format('YYYY-MM-DD'),
        };
    }

    leadReportDataRequest() {
        let queryParams: any = new QueryParamsModel(
            '',
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);

        queryParams = Object.assign(queryParams, this.customFilters);

        this.dataSource.loadBookingReportData(queryParams);
    }

    applyFilter() {
        // this.paginator.pageIndex = 0;
        // this.onHotelSelect();
        if (this.isVisibleAgents) {
            this.customFilters.travel_agent_id = this.travel_agent.value;
        }
        if (this.isVisibleCompanies) {
            this.customFilters.company_id = this.companyList.value;
        }
        this.loadReportData();
        // this.leadReportDataRequest();
    }

    applySearchFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    resetFilter() {
        this.companyList = new FormControl(null, []);
        this.travel_agent = new FormControl(null, []);
        this.isCollapsedFilter = true;
        this.initFilters();
        this.paginator.pageIndex = 0;
        this.leadReportDataRequest();
    }

    getHotels() {
        this.inquiryService.getHotels()
            .subscribe(response => {
                    this.hotelList = [];
                    response.items.forEach(itm => {
                        if (itm.is_enabled) {
                            itm.category = 'Active';
                            this.selectedHotelLists.push(itm);
                        } else {
                            itm.category = 'Disabled';
                        }
                        this.hotelList.push(itm);
                    });
                    this.hotelList = this.hotelList.sort((a, b) => a.category.localeCompare(b.category));
                    this.onHotelSelect();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    loadReportData() {
        let queryParams: any = new QueryParamsModel(
            '',
            this.sortOrderBy,
            this.sortField,
            1,
            10);

        queryParams = Object.assign(queryParams, this.customFilters);

        this.isLoading = true;
        this.reportService.inventoryReport(queryParams)
            .subscribe(response => {
                    this.reportData = [];
                    if (response) {
                        this.reportData = response.inventories;
                        this.dateRange = response.dates;
                    }
                    this.isLoading = false;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                    this.isLoading = false;
                });
    }

    exportReportData(type: string = 'pdf') {
        let queryParams: any = new QueryParamsModel(
            '',
            this.sortOrderBy,
            this.sortField,
            1,
            10);

        queryParams = Object.assign(queryParams, this.customFilters);
        queryParams.type = type;

        this.isLoading = true;
        this.reportService.inventoryReportExport(queryParams)
            .subscribe(response => {
                    this.isLoading = false;
                    window.open(response.message, '_blank');
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    selectedDate(value: any) {
        this.customFilters.from_date = value.start.format('YYYY-MM-DD');
        this.customFilters.to_date = value.end.format('YYYY-MM-DD');
    }


    getNextRoomDetails(record) {
        let plans;
        plans = [];
        record.booking_rooms.forEach((rm, index) => {
            if (index > 0) {
                plans.push(Object.assign({}, rm));
            }
        });

        return plans;
    }

    applySorting(field) {
        if (this.sortField === field) {
            if (this.sortOrderBy === 'asc') {
                this.sortOrderBy = 'desc';
            } else {
                this.sortOrderBy = 'asc';
            }
        } else {
            this.sortField = field;
            this.sortOrderBy = 'asc';
        }
        this.loadReportData();
    }

    onHotelSelect(item: any = {}) {
        this.customFilters.hotel_id = [];
        if (this.selectedHotelLists.length > 0) {
            this.selectedHotelLists.forEach(itm => {
                this.customFilters.hotel_id.push(itm.id);
            });
        }
        console.log(this.customFilters.hotel_id);
    }
}
