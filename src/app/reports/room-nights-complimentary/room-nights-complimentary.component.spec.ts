import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomNightsComplimentaryComponent } from './room-nights-complimentary.component';

describe('RoomNightsComplimentaryComponent', () => {
  let component: RoomNightsComplimentaryComponent;
  let fixture: ComponentFixture<RoomNightsComplimentaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomNightsComplimentaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomNightsComplimentaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
