import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {LayoutUtilsService, MessageType} from '../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {ToastrService} from 'ngx-toastr';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {QueryParamsModel} from '../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {SalesTargetDetailComponent} from './sales-target-detail/sales-target-detail.component';
import {SalesTargetService} from '../services/sales-target.service';
import {SalesTargetDataSource} from '../Models/data-sources/sales-target.dataSource';
import {TokenStorage} from '../core/auth/token-storage.service';

@Component({
    selector: 'm-sales-target',
    templateUrl: './sales-target.component.html',
    styleUrls: ['./sales-target.component.scss'],
    providers: [SalesTargetService]
})
export class SalesTargetComponent implements OnInit {

    salesTargets: any[] = [];
    dataSource: any = SalesTargetDataSource;
    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;
    loggedInUser: any = {};

    constructor(public dialog: MatDialog,
                private layoutUtilsService: LayoutUtilsService,
                private salesTargetService: SalesTargetService,
                private tokenStorage: TokenStorage,
                private toastService: ToastrService) {
        this.loggedInUser = this.tokenStorage.getUserDetail();
    }

    displayedColumns: string[] = [
        'select',
        'target_month',
        'assign_to',
        'room_nights',
        'revenue',
        'actions'
    ];

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadSalesTargetList())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadSalesTargetList();
                })
            )
            .subscribe();


        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadSalesTargetList();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SalesTargetDataSource(this.salesTargetService);
        this.dataSource.loadSalesTargets(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.salesTargets = res));
    }

    loadSalesTargetList() {
        const q = this.searchInput.nativeElement.value;
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadSalesTargets(queryParams);
        this.selection.clear();
    }

    openDetailForm(detail: any = {}) {
        const dialogRef = this.dialog.open(SalesTargetDetailComponent, {
            data: {
                currentRecord: detail,
                setting: {}
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadSalesTargetList();
            }
        });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.salesTargets.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.salesTargets.length) {
            this.selection.clear();
        } else {
            this.salesTargets.forEach(row => this.selection.select(row));
        }
    }

    deleteTarget(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.layoutUtilsService.showActionNotification('Not Implemented yet', MessageType.Delete);
        });
    }
}
