import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class DsrService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    save(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/dsrs', data);
    }

    get(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/dsrs', {params: params});
    }

    getHistory(id): Observable<any> {
        return this.http.get(this.API_URL + '/dsrs/history/' + id);
    }
}

