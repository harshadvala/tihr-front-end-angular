import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class HotelService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    getHotelList(): Observable<any> {
        return this.http.get(this.API_URL + '/hotels/list');
    }


    getHotels(): Observable<any> {
        return this.http.get(this.API_URL + '/hotels');
    }

    getHotelRoomTypes(hotelId): Observable<any> {
        return this.http.get(this.API_URL + '/hotels/room-types/' + hotelId);
    }

    saveHotelBank(hotelId, params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/hotels/' + hotelId + '/banks', params);
    }

    getHotelBanks(hotelId): Observable<any> {
        return this.http.get(this.API_URL + '/hotels/' + hotelId + '/banks');
    }

    deleteHotelBanks(bankId): Observable<any> {
        return this.http.delete(this.API_URL + '/hotels/' + bankId + '/banks');
    }

}

