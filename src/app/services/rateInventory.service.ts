import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class RateInventoryService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    getSettingList(): Observable<any> {
        return this.http.get(this.API_URL + '/settings/rate-inventory/filters');
    }

    getRateInventoryData(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/rate-inventory/inventory-data', params);
    }

    saveRateInventoryData(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/rate-inventory/save-inventory-data', params);
    }

    bulkUpdate(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/rate-inventory/bulk-update', params);
    }

    bulkDistributionUpdate(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/rate-inventory/dist-bulk-update', params);
    }

    fetchRateForBooking(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/rate-inventory/rate-for-booking', {params: params});
    }
}
