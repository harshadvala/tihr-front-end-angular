import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ReportService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    bookingOutStanding(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/booking-outstanding', params);
    }

    bookingOutStandingReportExport(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/export-booking-outstanding', params);
    }

    bookingDiscountReportExport(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/export-booking-discount', params);
    }

    bookingReport(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/booking-report', params);
    }

    bookingReportExport(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/export-booking-report', params);
    }

    inventoryReport(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/inventory-report', params);
    }

    inventoryReportExport(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/export-inventory-report', params);
    }

    bookingStatusReportData(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/booking-status-report', params);
    }

    bookingStatusReportExport(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/export-booking-status-report', params);
	}

    roomNightsReportExport(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/reports/export-room-night-report', params);
    }

}

