import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UserService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    getUserList(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/users/users-list', {params: params});
    }

    getUsers(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/users', {params: params});
    }

    save(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/users', data);
    }

    saveProfile(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/users/profile', data);
    }

    getUserImageUploadUrl(id: any = '') {
        return this.API_URL + '/users/image?user_id=' + id;
    }

    deleteUserImage(id): Observable<any> {
        return this.http.delete(this.API_URL + '/users/' + id + '/image');
    }
}

