import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingDataSource} from '../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {SettingsService} from '../../services/settings.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfigProvider} from '../../core/services/config-provider.service';

@Component({
    selector: 'm-application-settings',
    templateUrl: './application-settings.component.html',
    styleUrls: ['./application-settings.component.scss'],
    providers: [SettingsService, ConfigProvider]
})
export class ApplicationSettingsComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    rateTypeRecord: any = {};
    appSettings: any[] = [];
    dataSource: any = SettingDataSource;
    displayedColumns: string[] = [
        'name',
        'code'
    ];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private dialogRef: MatDialogRef<ApplicationSettingsComponent>,
                private dialog: MatDialog,
                private toastService: ToastrService,
                private layoutUtilsService: LayoutUtilsService,
                public  settingsService: SettingsService,
                private configProvider: ConfigProvider,
                private modalService: NgbModal) {
    }

    ngOnInit() {
        this.loadApplicationSettingsRequest();
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }


    loadApplicationSettingsRequest() {
        this.isLoading = true;
        this.settingsService.getApplicationSettings()
            .subscribe(response => {
                    this.appSettings = response.items;
                    this.isLoading = false;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveApplicationSettings(this.appSettings)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.configProvider.load();
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    getSettingType(name) {
        let type = '';
        switch (name) {
            case 'discount_approval_validation_rule':
                type = 'select';
                break;

            case 'allow_add_past_inq_for_days':
                type = 'number';
                break;

            case 'financial_year_end_date':
            case 'financial_year_start_date':
                type = 'date';
                break;

            default:
                type = 'string';
                break;
        }

        return type;
    }

    dateChangeEvent(rec) {
        rec['setting_value'] = rec['setting_value'].format('YYYY-MM-DD');
    }
}
