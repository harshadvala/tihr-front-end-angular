import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingsService} from '../../../services/settings.service';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {map, tap} from 'rxjs/operators';
import {merge, Observable, of} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {FormControl, Validators} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AmenityDetailComponent} from './amenity-detail/amenity-detail.component';

@Component({
    selector: 'm-amenities',
    templateUrl: './amenities.component.html',
    styleUrls: ['./amenities.component.scss'],
    providers: [SettingsService]
})
export class AmenitiesComponent implements OnInit {

    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    currentRecord: any = {};
    amenityTypes: any[] = [];
    amenities: any[] = [];
    dataSource: any = SettingDataSource;
    displayedColumns: string[] = [
        'name',
        'code',
        'type',
        'actions'
    ];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    amenity_type = new FormControl(null, [Validators.required]);
    amenityTypeDataSource: DataSource;

    constructor(private dialogRef: MatDialogRef<AmenitiesComponent>,
                private dialog: MatDialog,
                private toastService: ToastrService,
                private http: HttpClient,
                private layoutUtilsService: LayoutUtilsService,
                public  settingsService: SettingsService,
                private modalService: NgbModal) {


        const apiURL = environment.API_URL + '/settings/amenity-types?isList=true';

        this.amenityTypeDataSource = {
            displayValue(value: any): Observable<any | null> {
                console.log('finding display value for', value);
                if (typeof value === 'string') {
                    value = parseInt(value, 10);
                }
                if (typeof value !== 'number') {
                    return of(null);
                }

                return http.get<any>(apiURL).pipe(
                    map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))
                );
            },
            search(term: string): Observable<any[]> {
                return http.get<any[]>(apiURL, {
                    params: {
                        pageSize: '',
                        q: term || '',
                        _sort: 'name'
                    }
                }).pipe(
                    map(list => list.map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))));
            }
        };
    }

    ngOnInit() {
        this.getAmenityTypes();
        this.paginator.page
            .pipe(
                tap(() => this.loadAmenitiesRequest())
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadAmenitiesRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadAmenities(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.amenities = res));
    }


    loadAmenitiesRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadAmenities(queryParams);
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    openEntryForm(content, record: any = {}) {
        const dialogRef = this.dialog.open(AmenityDetailComponent, {
            width: '500px',
            data: {
                record: record,
                amenityTypes: this.amenityTypes
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.refreshCurrentPage();
            }
        });
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteAmenities(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        this.refreshCurrentPage();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

    refreshCurrentPage() {
        this.paginator.pageIndex -= 1;
        this.loadAmenitiesRequest();
    }

    getAmenityTypes() {
        this.settingsService.getAmenityTypes()
            .subscribe(response => {
                    this.amenityTypes = response.items;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }
}
