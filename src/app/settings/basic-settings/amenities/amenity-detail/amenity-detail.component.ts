import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {SettingsService} from '../../../../services/settings.service';

@Component({
    selector: 'm-amenity-detail',
    templateUrl: './amenity-detail.component.html',
    styleUrls: ['./amenity-detail.component.scss'],
    providers: [SettingsService]
})
export class AmenityDetailComponent implements OnInit {

    currentRecord: any = {};
    amenityTypes: any[] = [];
    isSaving: boolean = false;
    isLoading: boolean = false;

    constructor(private dialogRef: MatDialogRef<AmenityDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private modalService: NgbModal,
                private toastService: ToastrService,
                public  settingsService: SettingsService) {

        if (this.data) {
            this.amenityTypes = this.data.amenityTypes;
            if (this.data.record) {
                this.currentRecord = this.data.record;
            }
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveAmenities(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

}
