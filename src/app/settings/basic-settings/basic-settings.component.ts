import { InquiryStatusesComponent } from './inquiry-statuses/inquiry-statuses.component';
import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {RoomTypesComponent} from './room-types/room-types.component';
import {BedTypesComponent} from './bed-types/bed-types.component';
import {AmenityTypesComponent} from './amenity-types/amenity-types.component';
import {AmenitiesComponent} from './amenities/amenities.component';
import {MealPlansComponent} from './meal-plans/meal-plans.component';
import {HolidaysComponent} from './holidays/holidays.component';
import {DiscountsComponent} from './discounts/discounts.component';
import {TaxesComponent} from './taxes/taxes.component';
import {ExtrasComponent} from './extras/extras.component';
import {RatePlanTypesComponent} from './rate-plans/rate-plan-types.component';
import {ApplicationSettingsComponent} from '../application-settings/application-settings.component';
import {IndustriesComponent} from './industries/industries.component';
import {CategoriesComponent} from './categories/categories.component';
import {SourcesComponent} from './sources/sources.component';
import { InquiryTypesComponent } from './inquiry-types/inquiry-types.component';
import {DynamicDashboardsComponent} from './dynamic-dashboards/dynamic-dashboards.component';
import {InquiryPaymentStatusesComponent} from './inquiry-payment-statuses/inquiry-payment-statuses.component';
import {PaymentModesComponent} from './payment-modes/payment-modes.component';
import {UnitExpensesComponent} from '../property-masters/unit-expenses/unit-expenses.component';

@Component({
    selector: 'm-basic-settings',
    templateUrl: './basic-settings.component.html',
    styleUrls: ['./basic-settings.component.scss']
})
export class BasicSettingsComponent implements OnInit {

    constructor(public dialog: MatDialog) {
    }

    ngOnInit() {
    }

    showDynamicDashboards() {
        this.dialog.open(DynamicDashboardsComponent, {width: '600px', disableClose: true});
    }

    showUnitExpenses() {
        this.dialog.open(UnitExpensesComponent, {width: '600px', disableClose: true});
    }

    showIndustries() {
        this.dialog.open(IndustriesComponent, {width: '600px', disableClose: true});
    }

    showCategories() {
        this.dialog.open(CategoriesComponent, {width: '600px', disableClose: true});
    }

    showSources() {
        this.dialog.open(SourcesComponent, {width: '600px', disableClose: true});
	}

	showInquiryTypes() {
        this.dialog.open(InquiryTypesComponent, {width: '600px', disableClose: true});
	}

	showInquiryStatuses() {
        this.dialog.open(InquiryStatusesComponent, {width: '600px', disableClose: true});
	}

    showInquiryPaymentStatuses() {
        this.dialog.open(InquiryPaymentStatusesComponent, {width: '600px', disableClose: true});
    }

    showRoomTypes() {
        this.dialog.open(RoomTypesComponent, {width: '600px', disableClose: true});
    }

    showBedTypes() {
        this.dialog.open(BedTypesComponent, {width: '600px', disableClose: true});
    }

    showAmenityTypes() {
        this.dialog.open(AmenityTypesComponent, {width: '600px', disableClose: true});
    }

    showAmenities() {
        this.dialog.open(AmenitiesComponent, {width: '600px', disableClose: true});
    }

    showMealPlans() {
        this.dialog.open(MealPlansComponent, {width: '600px', disableClose: true});
    }

    showHolidays() {
        this.dialog.open(HolidaysComponent, {width: '600px', disableClose: true});
    }

    showDiscounts() {
        this.dialog.open(DiscountsComponent, {width: '850px', disableClose: true});
    }

    showTaxes() {
        this.dialog.open(TaxesComponent, {width: '600px', disableClose: true});
    }

    showExtras() {
        this.dialog.open(ExtrasComponent, {width: '850px', disableClose: true});
    }

    showRateType() {
        this.dialog.open(RatePlanTypesComponent, {width: '600px', disableClose: true});
    }

    showPaymentModes() {
        this.dialog.open(PaymentModesComponent, {width: '850px', disableClose: true});
    }

    showApplicationSettings() {
        this.dialog.open(ApplicationSettingsComponent, {width: '600px', disableClose: true});
    }
}
