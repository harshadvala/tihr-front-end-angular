import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {SettingsService} from '../../../services/settings.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';


@Component({
    selector: 'm-bed-types',
    templateUrl: './bed-types.component.html',
    styleUrls: ['./bed-types.component.scss'],
    providers: [SettingsService]
})
export class BedTypesComponent implements OnInit {

    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    currentRecord: any = {};
    bedTypes: any[] = [];
    dataSource: any = SettingDataSource;
    bedTypeModalRef: any;
    displayedColumns: string[] = [
        'name',
        'code',
        'actions'
    ];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;


    constructor(private dialogRef: MatDialogRef<BedTypesComponent>,
                private dialog: MatDialog,
                private toastService: ToastrService,
                private layoutUtilsService: LayoutUtilsService,
                public  settingsService: SettingsService,
                private modalService: NgbModal) {
    }

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadBedTypesRequest())
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadBedTypesRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadBedTypes(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.bedTypes = res));
    }


    loadBedTypesRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadBedTypes(queryParams);
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    openBedType(content, record: any = {}) {
        if (record) {
            this.currentRecord = record;
        } else {
            this.currentRecord = {};
        }
        this.bedTypeModalRef = this.modalService.open(content, {centered: true});
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveBedType(this.currentRecord)
            .subscribe(response => {
                    this.bedTypeModalRef.close();
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.currentRecord = {};
                    this.refreshCurrentPage();
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteBedType(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        this.refreshCurrentPage();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

    refreshCurrentPage() {
        this.paginator.pageIndex -= 1;
        this.loadBedTypesRequest();
    }
}
