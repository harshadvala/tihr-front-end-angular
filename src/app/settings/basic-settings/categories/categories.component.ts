import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingsService} from '../../../services/settings.service';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';

@Component({
    selector: 'm-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss'],
    providers: [SettingsService]
})
export class CategoriesComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    currentRecord: any = {status: true};
    categories: any[] = [];
    dataSource: any = SettingDataSource;
    entryModalRef: any;
    displayedColumns: string[] = ['name', 'status', 'actions'];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(
        private dialogRef: MatDialogRef<CategoriesComponent>,
        private dialog: MatDialog,
        private toastService: ToastrService,
        private layoutUtilsService: LayoutUtilsService,
        public settingsService: SettingsService,
        private modalService: NgbModal
    ) {
    }

    ngOnInit() {
        this.paginator.page
            .pipe(tap(() => this.loadCategoriesRequest()))
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadCategoriesRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadCategories(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.categories = res; // console.log(this.categories);
        });
    }

    loadCategoriesRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource.loadCategories(queryParams);
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    openEntryForm(content, record: any = {}) {
        if (record.id) {
            this.currentRecord = record;
        } else {
            this.currentRecord = {status: true};
        }
        this.entryModalRef = this.modalService.open(content, {
            size: 'sm',
            centered: true
        });
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveCategories(this.currentRecord).subscribe(
            response => {
                this.entryModalRef.close();
                this.isSaving = false;
                this.toastService.success('Saved Successfully', 'Success');
                this.currentRecord = {};
                this.refreshCurrentPage();
            },
            error => {
                this.isSaving = false;
                this.toastService.error(
                    error.error.message ? error.error.message : '',
                    'something went wrong'
                );
            }
        );
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteCategories(record.id).subscribe(
                response => {
                    this.isDeleting = false;
                    this.refreshCurrentPage();
                    this.toastService.success('Deleted Successfully', 'Done');
                },
                error => {
                    this.isDeleting = false;
                    this.toastService.error(
                        error.error.message ? error.error.message : '',
                        'something went wrong'
                    );
                }
            );
        });
    }

    refreshCurrentPage() {
        // this.paginator.pageIndex -= 1;
        this.loadCategoriesRequest();
    }
}
