import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingsService} from '../../../services/settings.service';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {UtilsService} from '../../../core/services/utils.service';
import {InquiryService} from '../../../inquiry/inquiry.service';
import {FormControl} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'm-discounts',
    templateUrl: './discounts.component.html',
    styleUrls: ['./discounts.component.scss'],
    providers: [SettingsService, InquiryService]
})
export class DiscountsComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    currentRecord: any = {};
    discounts: any[] = [];
    dataSource: any = SettingDataSource;
    detailModalRef: any;
    displayedColumns: string[] = [
        'name',
        'from',
        'to',
        'users',
        'status',
        'actions'
    ];

    userList: any[] = [];
    userListSettings: any = {};
    selectedUserLists = [];

    hotelList: any = [];
    hotelListSettings: any = {};
    selectedHotelLists = [];

    bookingTypesList: any = [];
    selectedBookingTypeLists = [];
    bookingTypesListsSettings: any = {};

    bookingStatusList: any = [];
    selectedBookingStatus = [];
    bookingStatusSettings: any = {};

    /*inquirySubStatusList: any = [];
    selectedSubStatusesLists = [];
    subStatusListSettings: any = {};*/

    sourceList: any = [];
    selectedSourcesLists = [];
    sourceSettings: any = {};

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private http: HttpClient,
                private dialogRef: MatDialogRef<DiscountsComponent>,
                private dialog: MatDialog,
                private toastService: ToastrService,
                private layoutUtilsService: LayoutUtilsService,
                public  settingsService: SettingsService,
                private modalService: NgbModal,
                private inquiryService: InquiryService,
                private utilsService: UtilsService) {
        this.userListSettings = this.utilsService.getMultiSelctConfig({
            text: 'Select User',
            badgeShowLimit: 1,
            groupBy: 'status_name'
        });
        this.hotelListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Hotels', labelKey: 'code', groupBy: 'status'});
        this.bookingTypesListsSettings = this.utilsService.getMultiSelctConfig({text: 'Select Booking Type', badgeShowLimit: 1});
        this.bookingStatusSettings = this.utilsService.getMultiSelctConfig({text: 'Select Booking Status', badgeShowLimit: 2});
        // this.subStatusListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Sub Status', badgeShowLimit: 1});
        this.sourceSettings = this.utilsService.getMultiSelctConfig({text: 'Select Source'});
    }

    ngOnInit() {
        this.getInquirySettingLists();

        this.paginator.page
            .pipe(
                tap(() => this.loadDiscountsRequest())
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadDiscountsRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadDiscounts(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.discounts = res));
    }

    loadDiscountsRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadDiscounts(queryParams);
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    isEmptyObject = obj => (obj && (Object.keys(obj).length === 0));

    openDetailForm(content, record: any = {}) {
        if (!this.isEmptyObject(record)) {
            // this.currentRecord = record;
            this.currentRecord.id = record.id;
            this.currentRecord.name = record.name;
            this.currentRecord.from = record.from;
            this.currentRecord.to = record.to;
            this.currentRecord.is_enabled = record.is_enabled;
            this.currentRecord.created_at = record.created_at;
            this.currentRecord.updated_at = record.updated_at;

            this.selectedUserLists = [];
            this.userList.filter(itm => {
                if (record.userIds.includes(itm.id)) {
                    this.selectedUserLists.push(itm);
                }
            });
            this.onUserSelect();

            this.selectedHotelLists = [];
            this.hotelList.filter(itm => {
                if (record.hotelIds.includes(itm.id)) {
                    this.selectedHotelLists.push(itm);
                }
            });
            this.onHotelSelect();

            this.selectedBookingTypeLists = [];
            this.bookingTypesList.filter(itm => {
                if (record.bookingTypeIds.includes(itm.id)) {
                    this.selectedBookingTypeLists.push(itm);
                }
            });
            this.onBookingTypesSelect();

            this.selectedBookingStatus = [];
            this.bookingStatusList.filter(itm => {
                if (record.bookingStatusIds.includes(itm.id)) {
                    this.selectedBookingStatus.push(itm);
                }
            });
            this.onBookingStatusSelect();

            this.selectedSourcesLists = [];
            this.sourceList.filter(itm => {
                if (record.sourceIds.includes(itm.id)) {
                    this.selectedSourcesLists.push(itm);
                }
            });
            this.onSourceSelectionEvent();
        } else {
            this.currentRecord = {
                is_enabled: true,
            };
            this.selectedUserLists = [];
            this.selectedHotelLists = [];
            this.selectedBookingTypeLists = [];
            this.selectedBookingStatus = [];
        }
        this.detailModalRef = this.modalService.open(content, {size: 'lg', centered: true});
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveDiscounts(this.currentRecord)
            .subscribe(response => {
                    this.detailModalRef.close();
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.currentRecord = {};
                    this.refreshCurrentPage();
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteDiscounts(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        this.refreshCurrentPage();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

    refreshCurrentPage() {
        this.paginator.pageIndex -= 1;
        this.loadDiscountsRequest();
    }

    getInquirySettingLists() {
        if (this.userList.length <= 0 ||
            this.hotelList.length <= 0 ||
            this.bookingTypesList.length <= 0 ||
            this.bookingStatusList.length < 1 ||
        /*this.inquirySubStatusList.length <= 0 ||*/
            this.sourceList.length <= 0
        ) {
            this.isLoading = true;
            this.inquiryService.getInquirySettingLists()
                .subscribe(response => {
                        this.userList = response.users;
                        this.hotelList = response.hotels;
                        this.bookingTypesList = response.types;
                        this.bookingStatusList = response.statuses;
                        /*this.inquirySubStatusList = response.subStatuses;*/
                        this.sourceList = response.sources;
                        this.isLoading = false;
                    },
                    error => {
                        this.isLoading = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                    });
        }

    }

    onUserSelect(item: any = {}) {
        this.currentRecord.users = [];
        if (this.selectedUserLists.length > 0) {
            this.selectedUserLists.forEach(itm => {
                this.currentRecord.users.push(itm);
            });
        }
    }

    onHotelSelect(item: any = {}) {
        this.currentRecord.hotels = [];
        if (this.selectedHotelLists.length > 0) {
            this.selectedHotelLists.forEach(itm => {
                this.currentRecord.hotels.push(itm);
            });
        }
    }

    onBookingTypesSelect(item: any = {}) {
        this.currentRecord.booking_types = [];
        if (this.selectedBookingTypeLists.length > 0) {
            this.selectedBookingTypeLists.forEach(itm => {
                this.currentRecord.booking_types.push(itm);
            });
        }
    }

    onBookingStatusSelect(item: any = {}) {
        this.currentRecord.booking_statuses = [];
        if (this.selectedBookingStatus.length > 0) {
            this.selectedBookingStatus.forEach(itm => {
                this.currentRecord.booking_statuses.push(itm);
            });
        }
    }

    /*onSubStatusSelect(item: any = {}) {
        this.currentRecord.sub_status_id = [];
        if (this.selectedSubStatusesLists.length > 0) {
            this.selectedSubStatusesLists.forEach(itm => {
                this.currentRecord.sub_status_id.push(itm.id);
            });
        }
    }*/

    onSourceSelectionEvent(item: any = {}) {
        this.currentRecord.sources = [];
        if (this.selectedSourcesLists.length > 0) {
            this.selectedSourcesLists.forEach(itm => {
                this.currentRecord.sources.push(itm);
            });
        }
    }
}
