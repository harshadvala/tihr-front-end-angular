import {Component, Inject, OnInit} from '@angular/core';
import {SettingsService} from '../../../../services/settings.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'm-extra-detail',
    templateUrl: './extra-detail.component.html',
    styleUrls: ['./extra-detail.component.scss'],
    providers: [SettingsService]
})
export class ExtraDetailComponent implements OnInit {
    isSaving: boolean = false;
    taxes: any = [];
    isLoading: boolean = false;
    currentRecord: any = {
        is_publish: true,
        is_enabled: true
    };

    constructor(private dialogRef: MatDialogRef<ExtraDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private modalService: NgbModal,
                private toastService: ToastrService,
                public  settingsService: SettingsService) {


        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
            }
        }
    }

    ngOnInit() {
        this.loadTaxes();
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveExtras(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    loadTaxes() {
        this.settingsService.getTaxes()
            .subscribe(response => {
                    this.taxes = response.items;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

}
