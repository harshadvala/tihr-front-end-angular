import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {SettingsService} from '../../../services/settings.service';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {ExtraDetailComponent} from './extra-detail/extra-detail.component';

@Component({
    selector: 'm-extras',
    templateUrl: './extras.component.html',
    styleUrls: ['./extras.component.scss'],
    providers: [SettingsService]
})
export class ExtrasComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    rateTypeRecord: any = {};
    roomTypes: any[] = [];
    dataSource: any = SettingDataSource;
    displayedColumns: string[] = [
        'name',
        'code',
        'price',
        'tax_id',
        'actions'
    ];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private dialogRef: MatDialogRef<ExtrasComponent>,
                private dialog: MatDialog,
                private toastService: ToastrService,
                private layoutUtilsService: LayoutUtilsService,
                public  settingsService: SettingsService) {
    }

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadExtrasRequest())
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadExtrasRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadExtras(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.roomTypes = res));
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    openDetailForm(record: any = {}) {
        const dialogRef = this.dialog.open(ExtraDetailComponent, {
            width: '500px',
            data: {record: record}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // this.paginator.pageIndex -= 1;
                this.loadExtrasRequest();
            }
        });
    }

    loadExtrasRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadExtras(queryParams);
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteExtras(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        // this.paginator.pageIndex -= 1;
                        this.loadExtrasRequest();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

}
