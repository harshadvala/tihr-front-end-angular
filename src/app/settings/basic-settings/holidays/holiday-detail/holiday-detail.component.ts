import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {SettingsService} from '../../../../services/settings.service';
import * as moment from 'moment';

@Component({
    selector: 'm-holiday-detail',
    templateUrl: './holiday-detail.component.html',
    styleUrls: ['./holiday-detail.component.scss'],
    providers: [SettingsService]
})
export class HolidayDetailComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    currentRecord: any = {
        from_date: moment().format('YYYY-MM-DD'),
        to_date: moment().format('YYYY-MM-DD'),
    };

    constructor(private dialogRef: MatDialogRef<HolidayDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private modalService: NgbModal,
                private toastService: ToastrService,
                public  settingsService: SettingsService) {

        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
            } else {
                this.currentRecord = {
                    from_date: moment().format('YYYY-MM-DD'),
                    to_date: moment().format('YYYY-MM-DD'),
                };
            }
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    saveHoliday() {
        this.isSaving = true;
        let data: any;
        data = {};
        data.id = (this.currentRecord.id) ? this.currentRecord.id : '';
        data.name = this.currentRecord.name;
        data.code = this.currentRecord.code;
        data.from_date = this.currentRecord.from_date;
        data.to_date = this.currentRecord.to_date;
        data.color_code = this.currentRecord.color_code;

        this.settingsService.saveHolidays(data)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    dateChangeEvent(type: string) {
        this.currentRecord[type] = this.currentRecord[type].format('YYYY-MM-DD HH:mm:ss');
    }
}
