import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {SettingsService} from '../../../services/settings.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {HolidayDetailComponent} from './holiday-detail/holiday-detail.component';

@Component({
    selector: 'm-holidays',
    templateUrl: './holidays.component.html',
    styleUrls: ['./holidays.component.scss'],
    providers: [SettingsService]
})
export class HolidaysComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    currentRecord: any = {};
    mealPlans: any[] = [];
    dataSource: any = SettingDataSource;
    entryModalRef: any;
    displayedColumns: string[] = [
        'name',
        'code',
        'from_date',
        'to_date',
        'actions'
    ];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;


    constructor(private dialogRef: MatDialogRef<HolidaysComponent>,
                private dialog: MatDialog,
                private toastService: ToastrService,
                private layoutUtilsService: LayoutUtilsService,
                public  settingsService: SettingsService,
                private modalService: NgbModal) {
    }

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadHolidaysRequest())
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadHolidaysRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadHolidays(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.mealPlans = res));
    }


    loadHolidaysRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadHolidays(queryParams);
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    openEntryForm(record: any = {}) {
        const dialogRef = this.dialog.open(HolidayDetailComponent, {
            width: '500px',
            data: {record: record}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.refreshCurrentPage();
            }
        });
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteHolidays(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        this.refreshCurrentPage();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

    refreshCurrentPage() {
        this.paginator.pageIndex -= 1;
        this.loadHolidaysRequest();
    }


}
