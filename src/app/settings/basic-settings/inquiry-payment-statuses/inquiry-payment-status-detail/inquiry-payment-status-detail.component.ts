import {Component, Inject, OnInit} from '@angular/core';
import {SettingsService} from '../../../../services/settings.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'm-payment-status-detail',
    templateUrl: './inquiry-payment-status-detail.component.html',
    styleUrls: ['./inquiry-payment-status-detail.component.scss'],
    providers: [SettingsService]
})
export class InquiryPaymentStatusDetailComponent implements OnInit {
    selectedTabIndex: number = 0;
    isSaving: boolean = false;
    isLoading: boolean = false;
    currentRecord: any = {payment_sub_statuses: []};
    currentInquiryPaymentSubStatus: any = {};

    constructor(
        private dialogRef: MatDialogRef<InquiryPaymentStatusDetailComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private modalService: NgbModal,
        private toastService: ToastrService,
        public settingsService: SettingsService
    ) {
        console.log(this.data);
        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
            }
        }
        if (!this.currentRecord.payment_sub_statuses) {
            this.currentRecord.payment_sub_statuses = [];
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveInquiryPaymentStatus(this.currentRecord).subscribe(
            response => {
                this.isSaving = false;
                this.toastService.success('Saved Successfully', 'Success');
                this.closeDialog(response);
            },
            error => {
                this.isSaving = false;
                this.toastService.error(
                    error.error.message ? error.error.message : '',
                    'something went wrong'
                );
            }
        );
    }

    statusTypeChangeEvent(updateField) {
        const updatedValue = this.currentRecord[updateField];
        this.currentRecord.is_confirmed = false;
        this.currentRecord.is_sold = false;
        this.currentRecord.is_lost = false;
        this.currentRecord.is_waitlisted = false;

        this.currentRecord[updateField] = updatedValue;
        if (this.currentRecord.is_sold) {
            this.currentRecord.is_confirmed = true;
        }
    }

    selectTab(index: number): void {
        this.selectedTabIndex = index;
    }

    resetInquiryPaymentSubStatus() {
        this.currentInquiryPaymentSubStatus = Object.assign({}, {});
    }

    editInquiryPaymentSubStatus(paymentSubStatus) {
        this.currentInquiryPaymentSubStatus = paymentSubStatus;
    }

    saveInquiryPaymentSubStatus(paymentSubStatus) {
        const index: number = this.currentRecord.payment_sub_statuses.indexOf(
            paymentSubStatus
        );
        if (index < 0) {
            this.currentRecord.payment_sub_statuses.push(paymentSubStatus);
        } else {
            this.currentRecord.payment_sub_statuses[index] = paymentSubStatus;
        }
        this.resetInquiryPaymentSubStatus();
    }

    deleteInquiryPaymentSubStatus(paymentSubStatus: any) {
        const index: number = this.currentRecord.payment_sub_statuses.indexOf(
            paymentSubStatus
        );
        if (index !== -1) {
            this.currentRecord.payment_sub_statuses.splice(index, 1);
        }
    }
}
