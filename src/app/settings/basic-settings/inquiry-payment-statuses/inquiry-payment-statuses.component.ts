import {InquiryPaymentStatusDetailComponent} from './inquiry-payment-status-detail/inquiry-payment-status-detail.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingsService} from '../../../services/settings.service';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';

@Component({
    selector: 'm-inquiry-statuses',
    templateUrl: './inquiry-payment-statuses.component.html',
    styleUrls: ['./inquiry-payment-statuses.component.scss'],
    providers: [SettingsService]
})
export class InquiryPaymentStatusesComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    currentRecord: any = {};
    paymentStatuses: any[] = [];
    dataSource: any = SettingDataSource;
    entryModalRef: any;
    displayedColumns: string[] = ['name', 'actions'];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(
        private dialogRef: MatDialogRef<InquiryPaymentStatusesComponent>,
        private dialog: MatDialog,
        private toastService: ToastrService,
        private layoutUtilsService: LayoutUtilsService,
        public settingsService: SettingsService,
        private modalService: NgbModal
    ) {
    }

    ngOnInit() {
        this.paginator.page
            .pipe(tap(() => this.loadInquiryPaymentStatusesRequest()))
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadInquiryPaymentStatusesRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadInquiryPaymentStatuses(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.paymentStatuses = res; // console.log(this.paymentStatuses);
        });
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    openDetailForm(record: any = {}) {
        const dialogRef = this.dialog.open(InquiryPaymentStatusDetailComponent, {
            width: '500px',
            data: {record: record}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.paginator.pageIndex -= 1;
                this.loadInquiryPaymentStatusesRequest();
            }
        });
    }

    loadInquiryPaymentStatusesRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource.loadInquiryPaymentStatuses(queryParams);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveInquiryPaymentStatus(this.currentRecord).subscribe(
            response => {
                this.entryModalRef.close();
                this.isSaving = false;
                this.toastService.success('Saved Successfully', 'Success');
                this.currentRecord = {};
                this.refreshCurrentPage();
            },
            error => {
                this.isSaving = false;
                this.toastService.error(
                    error.error.message ? error.error.message : '',
                    'something went wrong'
                );
            }
        );
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteInquiryPaymentStatus(record.id).subscribe(
                response => {
                    this.isDeleting = false;
                    this.refreshCurrentPage();
                    this.toastService.success('Deleted Successfully', 'Done');
                },
                error => {
                    this.isDeleting = false;
                    this.toastService.error(
                        error.error.message ? error.error.message : '',
                        'something went wrong'
                    );
                }
            );
        });
    }

    refreshCurrentPage() {
        // this.paginator.pageIndex -= 1;
        this.loadInquiryPaymentStatusesRequest();
    }
}
