import {Component, Inject, OnInit} from '@angular/core';
import {SettingsService} from '../../../../services/settings.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'm-inquiry-status-detail',
    templateUrl: './inquiry-status-detail.component.html',
    styleUrls: ['./inquiry-status-detail.component.scss'],
    providers: [SettingsService]
})
export class InquiryStatusDetailComponent implements OnInit {
    selectedTabIndex: number = 0;
    isSaving: boolean = false;
    isLoading: boolean = false;
    currentRecord: any = {sub_statuses: []};
    currentInquirySubStatus: any = {};

    constructor(
        private dialogRef: MatDialogRef<InquiryStatusDetailComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private modalService: NgbModal,
        private toastService: ToastrService,
        public settingsService: SettingsService
    ) {
        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
            }
        }
        if (!this.currentRecord.sub_statuses) {
            this.currentRecord.sub_statuses = [];
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveInquiryStatuses(this.currentRecord).subscribe(
            response => {
                this.isSaving = false;
                this.toastService.success('Saved Successfully', 'Success');
                this.closeDialog(response);
            },
            error => {
                this.isSaving = false;
                this.toastService.error(
                    error.error.message ? error.error.message : '',
                    'something went wrong'
                );
            }
        );
    }

    statusTypeChangeEvent(updateField) {
        const updatedValue = this.currentRecord[updateField];
        this.currentRecord.is_confirmed = false;
        this.currentRecord.is_sold = false;
        this.currentRecord.is_lost = false;
        this.currentRecord.is_waitlisted = false;

        this.currentRecord[updateField] = updatedValue;
        if (this.currentRecord.is_sold) {
            this.currentRecord.is_confirmed = true;
        }
    }

    selectTab(index: number): void {
        this.selectedTabIndex = index;
    }

    resetInquirySubStatus() {
        this.currentInquirySubStatus = Object.assign({}, {});
    }

    editInquirySubStatus(inquirySubStatus) {
        this.currentInquirySubStatus = inquirySubStatus;
    }

    saveInquirySubStatus(inquirySubStatus) {
        const index: number = this.currentRecord.sub_statuses.indexOf(
            inquirySubStatus
        );
        if (index < 0) {
            this.currentRecord.sub_statuses.push(inquirySubStatus);
        } else {
            this.currentRecord.sub_statuses[index] = inquirySubStatus;
        }
        this.resetInquirySubStatus();
    }

    deleteInquirySubStatus(inquirySubStatus: any) {
        const index: number = this.currentRecord.sub_statuses.indexOf(
            inquirySubStatus
        );
        if (index !== -1) {
            this.currentRecord.sub_statuses.splice(index, 1);
        }
    }
}
