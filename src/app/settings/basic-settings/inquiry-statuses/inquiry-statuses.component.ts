import {InquiryStatusDetailComponent} from './inquiry-status-detail/inquiry-status-detail.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import {SettingsService} from '../../../services/settings.service';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';

@Component({
    selector: 'm-inquiry-statuses',
    templateUrl: './inquiry-statuses.component.html',
    styleUrls: ['./inquiry-statuses.component.scss'],
    providers: [SettingsService]
})
export class InquiryStatusesComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    currentRecord: any = {};
    inquiryStatuses: any[] = [];
    dataSource: any = SettingDataSource;
    entryModalRef: any;
    displayedColumns: string[] = ['name', /*'description',*/ 'is_confirmed', 'is_sold', 'is_lost', 'is_waitlisted', 'actions'];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(
        private dialogRef: MatDialogRef<InquiryStatusesComponent>,
        private dialog: MatDialog,
        private toastService: ToastrService,
        private layoutUtilsService: LayoutUtilsService,
        public settingsService: SettingsService,
        private modalService: NgbModal
    ) {
    }

    ngOnInit() {
        this.paginator.page
            .pipe(tap(() => this.loadInquiryStatusesRequest()))
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadInquiryStatusesRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadInquiryStatuses(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.inquiryStatuses = res; // console.log(this.inquiryStatuses);
        });
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    openDetailForm(record: any = {}) {
        const dialogRef = this.dialog.open(InquiryStatusDetailComponent, {
            width: '500px',
            data: {record: record}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.paginator.pageIndex -= 1;
                this.loadInquiryStatusesRequest();
            }
        });
    }

    loadInquiryStatusesRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource.loadInquiryStatuses(queryParams);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveInquiryStatuses(this.currentRecord).subscribe(
            response => {
                this.entryModalRef.close();
                this.isSaving = false;
                this.toastService.success('Saved Successfully', 'Success');
                this.currentRecord = {};
                this.refreshCurrentPage();
            },
            error => {
                this.isSaving = false;
                this.toastService.error(
                    error.error.message ? error.error.message : '',
                    'something went wrong'
                );
            }
        );
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteInquiryStatuses(record.id).subscribe(
                response => {
                    this.isDeleting = false;
                    this.refreshCurrentPage();
                    this.toastService.success('Deleted Successfully', 'Done');
                },
                error => {
                    this.isDeleting = false;
                    this.toastService.error(
                        error.error.message ? error.error.message : '',
                        'something went wrong'
                    );
                }
            );
        });
    }

    refreshCurrentPage() {
        // this.paginator.pageIndex -= 1;
        this.loadInquiryStatusesRequest();
    }
}
