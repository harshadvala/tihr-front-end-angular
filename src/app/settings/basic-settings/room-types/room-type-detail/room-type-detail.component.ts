import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {SettingsService} from '../../../../services/settings.service';

@Component({
    selector: 'm-room-type-detail',
    templateUrl: './room-type-detail.component.html',
    styleUrls: ['./room-type-detail.component.scss'],
    providers: [SettingsService]
})

export class RoomTypeDetailComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    rateTypeRecord: any = {
        color_code: 'red'
    };

    constructor(private dialogRef: MatDialogRef<RoomTypeDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private modalService: NgbModal,
                private toastService: ToastrService,
                public  settingsService: SettingsService) {

        if (this.data) {
            this.rateTypeRecord = this.data.record;
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveRoomType(this.rateTypeRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }
}
