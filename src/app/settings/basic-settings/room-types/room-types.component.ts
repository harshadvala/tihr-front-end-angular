import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSort} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RoomTypeDetailComponent} from './room-type-detail/room-type-detail.component';
import {SettingsService} from '../../../services/settings.service';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';

@Component({
    selector: 'm-room-types',
    templateUrl: './room-types.component.html',
    styleUrls: ['./room-types.component.scss'],
    providers: [SettingsService]
})
export class RoomTypesComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    rateTypeRecord: any = {};
    roomTypes: any[] = [];
    dataSource: any = SettingDataSource;
    displayedColumns: string[] = [
        'name',
        'code',
        'actions'
    ];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private dialogRef: MatDialogRef<RoomTypesComponent>,
                private dialog: MatDialog,
                private toastService: ToastrService,
                private layoutUtilsService: LayoutUtilsService,
                public  settingsService: SettingsService,
                private modalService: NgbModal) {
    }

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadRoomTypesRequest())
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadRoomTypesRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadRoomTypes(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.roomTypes = res));
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    openHistory(content) {
        this.modalService.open(content, {centered: true}).result.then((result) => {
            // this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    openRoomTypeDetail(record: any = {}) {
        const dialogRef = this.dialog.open(RoomTypeDetailComponent, {
            width: '500px',
            data: {record: record}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.paginator.pageIndex -= 1;
                this.loadRoomTypesRequest();
            }
        });
    }

    loadRoomTypesRequest() {
        const q = '';
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadRoomTypes(queryParams);
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteRoomType(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        this.paginator.pageIndex -= 1;
                        this.loadRoomTypesRequest();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }
}
