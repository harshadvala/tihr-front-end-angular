import {Component, Inject, OnInit} from '@angular/core';
import {SettingsService} from '../../../../services/settings.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import * as moment from 'moment';

@Component({
    selector: 'm-tax-detail',
    templateUrl: './tax-detail.component.html',
    styleUrls: ['./tax-detail.component.scss'],
    providers: [SettingsService]
})
export class TaxDetailComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    currentRecord: any = {
        ranges: []
    };
    minDate = moment().format('YYYY-MM-DD');
    taxTypes: any = [
        'Flat',
        'Percentage',
        'Range'
    ];

    constructor(private dialogRef: MatDialogRef<TaxDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private modalService: NgbModal,
                private toastService: ToastrService,
                public  settingsService: SettingsService) {


        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
            }
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveTaxes(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    dateChangeEvent(type: string) {
        this.currentRecord[type] = this.currentRecord[type].format('YYYY-MM-DD');
    }

    addTaxRange() {
        this.currentRecord.ranges.push({
            from_amount: null,
            to_amount: null,
            value: null
        });
    }

    removeTaxRange(index) {
        this.currentRecord.ranges.splice(index, 1);
    }
}
