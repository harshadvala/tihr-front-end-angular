import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CMMappingService} from '../../services/cmMapping.service';
import {ToastrService} from 'ngx-toastr';
import {HotelService} from '../../services/hotel.service';
import {SettingDataSource} from '../../Models/data-sources/masters/setting.dataSource';
import {MatPaginator, MatSort} from '@angular/material';
import {fromEvent, merge} from 'rxjs';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {SettingsService} from '../../services/settings.service';
import {SelectionModel} from '@angular/cdk/collections';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

@Component({
    selector: 'm-cm-logs',
    templateUrl: './cm-logs.component.html',
    styleUrls: ['./cm-logs.component.scss'],
    providers: [CMMappingService, HotelService, SettingsService]
})
export class CMLogsComponent implements OnInit {
    currentLog = {};
    cmLogs: any[] = [];
    hotels: any[] = [];
    itemsPerPage: number = 10;
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    isCollapsedFilter: boolean = true;
    logFilters: any = {};
    dataSource: any = SettingDataSource;
    selection = new SelectionModel<any>(true, []);

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    dateOptions: any = {
        locale: {format: 'DD-MM-YYYY'},
        alwaysShowCalendars: false,
        startDate: moment(),
        endDate: moment(),
        opens: 'center',
    };

    displayedColumns: string[] = [
        'select',
        'hotel_code',
        'user_name',
		'from_date',
		'value',
        'operation_type',
        // 'request_name',
        'booking_id',
        'inquiry_id',
        'room_type_name',
        'status_name',
        'created_at',
        'actions'
    ];
    logStatuses: any = [
        {
            id: 0,
            name: 'In Progress'
        },
        {
            id: 1,
            name: 'Success'
        },
        {
            id: 2,
            name: 'Failed'
        },
        {
            id: 3,
            name: 'Queued'
        }
    ];

    operationTypes: any = [
        {
            id: 'UpdateRoomRates',
            name: 'Update Room Rates'
        },
        {
            id: 'UpdateStopSell',
            name: 'Update Stop Sell'
        },
        {
            id: 'Bookings',
            name: 'Bookings'
        },
        {
            id: 'Push',
            name: 'Push / Web Hook'
        },
        {
            id: 'BookingRecdNotification',
            name: 'Booking Notification'
        },
        {
            id: 'UpdateAvailability',
            name: 'Update Availability'
        },
    ];

    constructor(private toastService: ToastrService,
                private cmMappingService: CMMappingService,
                public  settingsService: SettingsService,
                private modalService: NgbModal,
                private hotelService: HotelService) {
    }

    ngOnInit() {
        this.getHotels();

        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadLogRequests();
                })
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadLogRequests();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadChannelManagerLogs(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.cmLogs = res));
    }

    getHotels() {
        this.hotelService.getHotelList()
            .subscribe(response => {
                    this.hotels = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    loadLogRequests() {
        const q = this.searchInput.nativeElement.value;
        let queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);

        queryParams = Object.assign(queryParams, this.logFilters);

        this.dataSource.loadChannelManagerLogs(queryParams);
    }

    applyFilter() {
        this.paginator.pageIndex = 0;
        this.loadLogRequests();
    }

    resetFilter() {
        this.paginator.pageIndex = 0;
        this.searchInput.nativeElement.value = '';
        this.logFilters = {};
        this.isCollapsedFilter = true;
        this.loadLogRequests();
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.cmLogs.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.cmLogs.length) {
            this.selection.clear();
        } else {
            this.cmLogs.forEach(row => this.selection.select(row));
        }
    }

    openDetail(model, content) {
        this.currentLog = content;
        this.getLogDetail(content);
        this.modalService.open(model, {size: 'lg', centered: true});
    }

    clearDateFilter() {
        this.logFilters.date_filter = false;
    }

    selectDateForFilter(value: any) {
        this.logFilters.created_from = value.start.format('YYYY-MM-DD');
        this.logFilters.created_to = value.end.format('YYYY-MM-DD');
    }

    getLogDetail(record: any) {
        if (record.request && record.response) {
            return;
        }
        this.isLoading = true;
        this.settingsService.getCMLogDetails(record.id)
            .subscribe(response => {
                    record.request = response.request;
                    record.response = response.response;
                    this.isLoading = false;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                    this.isLoading = false;
                });
    }
}
