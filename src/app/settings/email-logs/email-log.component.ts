import {Component, OnInit, ViewChild} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {tap} from 'rxjs/operators';
import {SettingDataSource} from '../../Models/data-sources/masters/setting.dataSource';
import {CommonService} from '../../services/common.service';
import {SettingsService} from '../../services/settings.service';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {LogDetailComponent} from './log-detail/log-detail.component';

@Component({
    selector: 'm-email-log',
    templateUrl: './email-log.component.html',
    styleUrls: ['./email-log.component.scss'],
    providers: [CommonService, SettingsService]
})
export class EmailLogComponent implements OnInit {
    emailLogs: any[] = [];
    dataSource: any = SettingDataSource;
    isDeleting: boolean = false;
    isSaving: boolean = false;
    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(public dialog: MatDialog,
                public settingService: SettingsService) {
    }

    displayedColumns: string[] = [
        'subject',
        'created_at',
        'actions'
    ];

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadEmailLogRequest())
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'desc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingService);
        this.dataSource.loadEmailLog(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.emailLogs = res;
        });
    }

    loadEmailLogRequest() {
        const q = ''; // this.searchInput.nativeElement.value;
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadEmailLog(queryParams);
        this.selection.clear();
    }

    openDetailForm(detail: any = {}) {
        const dialogRef = this.dialog.open(LogDetailComponent, {
            width: '900px',
            data: {
                record: detail,
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadEmailLogRequest();
            }
        });
    }

    resetFilter() {
        this.paginator.pageIndex = 0;
        this.loadEmailLogRequest();
    }
}
