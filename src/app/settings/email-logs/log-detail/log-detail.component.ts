import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'm-log-detail',
    templateUrl: './log-detail.component.html',
    styleUrls: ['./log-detail.component.scss']
})
export class LogDetailComponent implements OnInit {
    isSaving: boolean = false;
    currentRecord: any = {};

    constructor(private dialogRef: MatDialogRef<LogDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
            }
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }
}
