import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CommonService} from '../../services/common.service';
import {SettingsService} from '../../services/settings.service';
import {SettingDataSource} from '../../Models/data-sources/masters/setting.dataSource';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {ToastrService} from 'ngx-toastr';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {TemplateDetailComponent} from './template-detail/template-detail.component';
import {TemplateSettingsComponent} from './template-settings/template-settings.component';

@Component({
    selector: 'm-email-templates',
    templateUrl: './email-templates.component.html',
    styleUrls: ['./email-templates.component.scss'],
    providers: [CommonService, SettingsService]
})
export class EmailTemplatesComponent implements OnInit {
    dataSource: any = SettingDataSource;
    isDeleting: boolean = false;
    isSaving: boolean = false;
    hotels: any[] = [];
    designations: any[] = [];
    departments: any[] = [];
    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    constructor(public dialog: MatDialog,
                private layoutUtilsService: LayoutUtilsService,
                private commonService: CommonService,
                private toastService: ToastrService,
                public settingService: SettingsService) {
    }

    displayedColumns: string[] = [
        'select',
        'name',
        'actions'
    ];

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadTemplatesRequest())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadTemplatesRequest();
                })
            )
            .subscribe();


        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadTemplatesRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingService);
        this.dataSource.loadEmailTemplates(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.hotels = res;
        });

    }

    loadTemplatesRequest() {
        const q = this.searchInput.nativeElement.value;
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadEmailTemplates(queryParams);
        this.selection.clear();
    }

    openDetailForm(detail: any = {}) {
        const dialogRef = this.dialog.open(TemplateDetailComponent, {
            width: '900px',
            data: {
                record: detail,
                designations: this.designations,
                departments: this.departments
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadTemplatesRequest();
            }
        });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.hotels.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.hotels.length) {
            this.selection.clear();
        } else {
            this.hotels.forEach(row => this.selection.select(row));
        }
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingService.deleteEmailTemplates(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        this.paginator.pageIndex -= 1;
                        this.loadTemplatesRequest();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

    copyTemplate(template) {
        template = Object.assign({}, template);
        template.name = template.name + '- Copy';
        template.email_subject = template.email_subject + '- Copy';
        delete template.id;
        delete template.key;

        this.isSaving = true;
        this.settingService.saveEmailTemplates(template)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Copied Successfully', 'Success');
                    this.loadTemplatesRequest();
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    openTemplateSettings(detail: any = {}) {
        const dialogRef = this.dialog.open(TemplateSettingsComponent , {
            width: '750px',
            data: {}
        });
    }
}
