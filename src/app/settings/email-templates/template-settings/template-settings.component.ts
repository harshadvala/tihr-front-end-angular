import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {UtilsService} from '../../../core/services/utils.service';
import {SettingsService} from '../../../services/settings.service';

@Component({
    selector: 'm-template-settings',
    templateUrl: './template-settings.component.html',
    styleUrls: ['./template-settings.component.scss'],
    providers: [SettingsService]
})

export class TemplateSettingsComponent implements OnInit {

    isSaving: boolean = false;
    isLoading: boolean = false;
    templateAssignments = [];
    emailTemplates = [];

    constructor(private dialogRef: MatDialogRef<TemplateSettingsComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private toastService: ToastrService,
                private utilsService: UtilsService,
                private  settingsService: SettingsService) {

        if (this.data) {
            if (this.data) {
                this.emailTemplates = this.data.templates;
            }
        }
    }

    ngOnInit() {
        this.loadTemplates();
    }

    loadAssignments() {
        this.isLoading = true;
        this.settingsService.getEmailAssignments()
            .subscribe(response => {
                    this.templateAssignments = response;
                    this.isLoading = false;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    loadTemplates() {
        this.isLoading = true;
        this.settingsService.getEmailTemplates()
            .subscribe(response => {
                    this.emailTemplates = response.items;
                    this.isLoading = false;
                    this.loadAssignments();
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveEmailAssignments(this.templateAssignments)
            .subscribe(response => {
                    this.templateAssignments = response;
                    this.isSaving = false;
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

}
