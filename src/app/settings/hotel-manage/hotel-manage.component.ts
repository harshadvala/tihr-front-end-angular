import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CommonService} from '../../services/common.service';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSlideToggleChange, MatSort} from '@angular/material';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {ToastrService} from 'ngx-toastr';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {SettingDataSource} from '../../Models/data-sources/masters/setting.dataSource';
import {SettingsService} from '../../services/settings.service';
import {HotelDetailComponent} from './hotel-detail/hotel-detail.component';

@Component({
    selector: 'm-hotel-manage',
    templateUrl: './hotel-manage.component.html',
    styleUrls: ['./hotel-manage.component.scss'],
    providers: [CommonService, SettingsService]
})
export class HotelManageComponent implements OnInit {
    dataSource: any = SettingDataSource;
    hotels: any[] = [];
    designations: any[] = [];
    departments: any[] = [];
    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    constructor(public dialog: MatDialog,
                private layoutUtilsService: LayoutUtilsService,
                private commonService: CommonService,
                private toastService: ToastrService,
                public settingService: SettingsService) {

    }

    displayedColumns: string[] = [
        'select',
        'name',
        'code',
        'email',
        'org_code',
        'is_enabled',
        'actions'
    ];

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadHotelRequest())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadHotelRequest();
                })
            )
            .subscribe();


        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadHotelRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingService);
        this.dataSource.loadHotels(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.hotels = res;
        });

    }

    onStatusChange(currentRecord, event: MatSlideToggleChange) {
        const dialogRef = this.layoutUtilsService.deleteElement(
            'Confirm Operation',
            'Are you sure to perform action?',
            'saving...',
            'Yes',
            'No'
        );
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                currentRecord.is_enabled = !event.checked;
                return;
            }
            this.save(currentRecord, event.checked);
        });
    }

    save(currentRecord, new_is_enabled) {
        currentRecord.isSaving = true;
        currentRecord.is_enabled = new_is_enabled;
        this.settingService.saveHotels(currentRecord)
            .subscribe(response => {
                    currentRecord.isSaving = false;
                    currentRecord.is_enabled = response.is_enabled;
                    this.toastService.success('Saved Successfully', 'Success');
                },
                error => {
                    currentRecord.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    loadHotelRequest() {
        const q = this.searchInput.nativeElement.value;
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadHotels(queryParams);
        this.selection.clear();
    }

    openDetailForm(detail: any = {}) {
        const dialogRef = this.dialog.open(HotelDetailComponent, {
            data: {
                record: detail,
                designations: this.designations,
                departments: this.departments
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadHotelRequest();
            }
        });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.hotels.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.hotels.length) {
            this.selection.clear();
        } else {
            this.hotels.forEach(row => this.selection.select(row));
        }
    }

    deleteUser(user) {
        //
    }
}
