import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {SettingsService} from '../../../services/settings.service';

@Component({
    selector: 'm-organization-detail',
    templateUrl: './organization-detail.component.html',
    styleUrls: ['./organization-detail.component.scss'],
    providers: [SettingsService]
})
export class OrganizationDetailComponent implements OnInit {
    selectedTabIndex: number = 0;
    isSaving: boolean = false;
    currentRecord: any = {};

    constructor(private dialogRef: MatDialogRef<OrganizationDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private toastService: ToastrService,
                private  settingsService: SettingsService) {

        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
            }
        }
    }

    ngOnInit() {

    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveOrganization(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }
}
