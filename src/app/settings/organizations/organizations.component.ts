import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CommonService} from '../../services/common.service';
import {SettingsService} from '../../services/settings.service';
import {SettingDataSource} from '../../Models/data-sources/masters/setting.dataSource';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {ToastrService} from 'ngx-toastr';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {OrganizationDetailComponent} from './organization-detail/organization-detail.component';

@Component({
    selector: 'm-organizations',
    templateUrl: './organizations.component.html',
    styleUrls: ['./organizations.component.scss'],
    providers: [CommonService, SettingsService]
})
export class OrganizationsComponent implements OnInit {
    dataSource: any = SettingDataSource;
    organizations: any[] = [];
    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    constructor(public dialog: MatDialog,
                private layoutUtilsService: LayoutUtilsService,
                private commonService: CommonService,
                private toastService: ToastrService,
                public settingService: SettingsService) {

    }

    displayedColumns: string[] = [
        'select',
        'name',
        'code',
        'address',
        'actions'
    ];

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadOrganizationsRequest())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadOrganizationsRequest();
                })
            )
            .subscribe();


        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadOrganizationsRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingService);
        this.dataSource.loadOrganizations(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.organizations = res;
        });

    }

    loadOrganizationsRequest() {
        const q = this.searchInput.nativeElement.value;
        const queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);
        this.dataSource.loadOrganizations(queryParams);
        this.selection.clear();
    }

    openDetailForm(detail: any = {}) {
        const dialogRef = this.dialog.open(OrganizationDetailComponent, {
            width: '700px',
            data: {
                record: detail
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadOrganizationsRequest();
            }
        });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.organizations.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.organizations.length) {
            this.selection.clear();
        } else {
            this.organizations.forEach(row => this.selection.select(row));
        }
    }

    deleteUser(user) {
        //
    }
}
