import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {SettingsService} from '../../../../services/settings.service';
import {LayoutUtilsService} from '../../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';

@Component({
    selector: 'm-copy-room',
    templateUrl: './copy-room.component.html',
    styleUrls: ['./copy-room.component.scss'],
    providers: [SettingsService]
})
export class CopyRoomComponent implements OnInit {
    isSaving: boolean = false;
    sourceRoom: any = {};
    isLoading: boolean = false;
    roomSettings: any = {};
    currentRecord: any = {};
    copyOptions: any = [
        'Range',
        'Custom'
    ];


    constructor(private dialogRef: MatDialogRef<CopyRoomComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private layoutUtilsService: LayoutUtilsService,
                private toastService: ToastrService,
                public  settingsService: SettingsService) {

        if (this.data) {
            if (this.data.record.id) {
                this.sourceRoom = Object.assign({}, this.data.record);
                const sourceRoom = Object.assign({}, this.data.record);
                this.currentRecord = {
                    sourceRoomId: sourceRoom.id,
                    name: sourceRoom.name,
                    hotel_id: sourceRoom.hotel_id,
                    room_type_id: sourceRoom.room_type_id,
                    bed_type_id: sourceRoom.bed_type_id,
                    copy_type: 'Range',
                };
            }
            this.roomSettings = data.settings;
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        if (this.sourceRoom.hotel_id !== this.currentRecord.hotel_id ||
            this.sourceRoom.room_type_id !== this.currentRecord.room_type_id ||
            this.sourceRoom.bed_type_id !== this.currentRecord.bed_type_id) {
            const dialogRef = this.layoutUtilsService.deleteElement('Confirm',
                'Values of Hotel, Room type or Bed type have changed. Do you really want to continue?',
                '....', 'Yes', 'No');
            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.copyRoom();
                }
            });
        } else {
            this.copyRoom();
        }
    }

    copyRoom() {
        this.isSaving = true;
        this.settingsService.copyHotelRooms(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Copied Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }
}
