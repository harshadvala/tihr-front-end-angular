import {Component, Inject, OnInit} from '@angular/core';
import {SettingsService} from '../../../../services/settings.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {UtilsService} from '../../../../core/services/utils.service';
import * as moment from 'moment';

@Component({
    selector: 'm-room-detail',
    templateUrl: './room-detail.component.html',
    styleUrls: ['./room-detail.component.scss'],
    providers: [SettingsService]
})
export class RoomDetailComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    currentRecord: any = {
        is_enabled: true,
        is_non_smoking: true,
        room_as: 'Normal',
        suite_name: 'Suit1',
        disable_from: null,
        disable_to: null
    };
    roomSettings: any = {};
    roomAs: any = [
        'Normal',
        'Suite',
        'Suite Child'
    ];
    suitNames: any = [
        'Suit1',
        'Suite2'
    ];

    disableDates: any = false;
    dateOptions: any = {};

    constructor(private dialogRef: MatDialogRef<RoomDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private modalService: NgbModal,
                private toastService: ToastrService,
                private utilsService: UtilsService,
                public  settingsService: SettingsService) {

        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
            }
            this.roomSettings = data.settings;
            this.disableDates = !!(this.currentRecord.disable_from && this.currentRecord.disable_to);
        }
        let disableFrom: any = moment();
        let disableTo: any = moment().add(7, 'days');
        if (this.currentRecord.disable_from && this.currentRecord.disable_to) {
            const disable_from = moment(this.currentRecord.disable_from, 'YYYY/MM/DD');
            const disable_to = moment(this.currentRecord.disable_to, 'YYYY/MM/DD');
            disableFrom = moment(disable_from).format('DD-MM-YYYY');
            disableTo = moment(disable_to).format('DD-MM-YYYY');
        }
        this.dateOptions = this.utilsService.getCalenderConfig({
            locale: {format: 'DD-MM-YYYY'},
            startDate: disableFrom,
            endDate: disableTo,
            opens: 'center',
            drops: 'up'
        });
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        if (!this.disableDates || this.currentRecord.is_enabled) {
            this.currentRecord.disable_from = null;
            this.currentRecord.disable_to = null;
        }
        this.isSaving = true;
        this.settingsService.saveHotelRooms(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    clearDisableDates() {
        this.disableDates = false;
    }

    selectDisableDates(value: any) {
        this.currentRecord.disable_from = value.start.format('YYYY-MM-DD');
        this.currentRecord.disable_to = value.end.format('YYYY-MM-DD');
    }
}
