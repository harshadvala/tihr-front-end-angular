import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';

@Component({
    selector: 'm-property-masters',
    templateUrl: './property-masters.component.html',
    styleUrls: ['./property-masters.component.scss']
})
export class PropertyMastersComponent implements OnInit {

    constructor(private dialog: MatDialog) {
    }

    ngOnInit() {
    }

}
