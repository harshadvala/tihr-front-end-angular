import {Component, Inject, OnInit} from '@angular/core';
import {RateInventoryService} from '../../../services/rateInventory.service';
import {SettingsService} from '../../../services/settings.service';
import * as moment from 'moment';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';

@Component({
    selector: 'm-update-wizard',
    templateUrl: './update-wizard.component.html',
    styleUrls: ['./update-wizard.component.scss'],
    providers: [RateInventoryService, SettingsService]
})
export class UpdateWizardComponent implements OnInit {
    selectedIndex: number = 0;
    selectedPlansCount = 0;
    isLoading: boolean = false;
    isSaving: boolean = false;
    inventorySources: any[] = [];
    ratePlans: any[] = [];
    hotels: any[] = [];
    roomTypes: any[] = [];
    minFromDate: any = moment();
    minToDate: any = moment();
    bulkUpdate: any = {
        rate_type: 'rate',
        rate_source_id: null,
        hotel_id: null,
        room_type_id: '',
        fromDate: moment().format('YYYY-MM-DD'),
        toDate: moment().format('YYYY-MM-DD'),
        rate_plan_id: [],
        rate_plans: [],
        selected_room_plans: [],
        update_field: 'rate'
    };
    selectedDays: any = [
        {
            name: 'Mon',
            is_selected: true
        },
        {
            name: 'Tue',
            is_selected: true
        }, {
            name: 'Wed',
            is_selected: true
        }, {
            name: 'Thu',
            is_selected: true
        }, {
            name: 'Fri',
            is_selected: true
        }, {
            name: 'Sat',
            is_selected: true
        }, {
            name: 'Sun',
            is_selected: true
        }
    ];

    constructor(public dialogRef: MatDialogRef<UpdateWizardComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private toastService: ToastrService,
                private settingsService: SettingsService,
                private layoutUtilsService: LayoutUtilsService,
                private rateInventoryService: RateInventoryService) {
        if (this.data) {
            this.hotels = this.data.hotels;
            this.inventorySources = this.data.inventorySources;
            this.roomTypes = this.data.roomTypes;
            if (this.data.filters) {
                this.bulkUpdate = Object.assign(this.bulkUpdate, this.data.filters);
                if (this.bulkUpdate.hotel_id) {
                    this.loadRatePlans();
                }
            }
        }
    }

    ngOnInit() {
        //
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    dateChangeEvent(type: string) {
        switch (type) {
            case 'fromDate':
                this.minToDate = this.bulkUpdate[type];
                break;
        }
        this.bulkUpdate[type] = this.bulkUpdate[type].format('YYYY-MM-DD');
    }

    save() {
        let reqData: any = {};
        reqData = Object.assign({}, this.bulkUpdate);

        this.bulkUpdate.rate_plans.forEach(rtPln => {
            if (rtPln.is_selected === true) {
                let updateMealPlans;
                updateMealPlans = [];
                rtPln.mealplans.forEach(mlPlan => {
                    if (mlPlan.is_selected === true) {
                        updateMealPlans.push(mlPlan);
                    }
                });

                rtPln.mealplans = updateMealPlans;
            }
        });

        reqData.days = [];

        this.selectedDays.forEach(itm => {
            if (itm.is_selected === true) {
                reqData.days.push(itm.name);
            }
        });

        const selHotel = this.hotels.find(itm => {
            return itm.id === this.bulkUpdate.hotel_id;
        });

        // const selRoomType = this.roomTypes.find(itm => {
        //     return itm.id === this.bulkUpdate.room_type_id;
        // });
        //
        // const selRatePlans = this.ratePlans.filter(itm => {
        //     const isExist: boolean = this.bulkUpdate.rate_plan_id.indexOf(itm);
        //     return isExist;
        // });
        let planStr = '';
        if (this.bulkUpdate.selected_room_plans && this.bulkUpdate.update_field !== 'inventory') {
            planStr += '<ul style="list-style: square; padding-left: 10px;">';
            this.bulkUpdate.selected_room_plans.forEach(rmType => {
                let planNames: any[];
                planNames = [];
                rmType.plans.forEach(itm => {
                    planNames.push(itm.name);
                });
                const planNamesStr = planNames.join(', ');
                planStr += '<li><b>' + rmType.name + '</b>: ' + planNamesStr + ' </li>';
            });
            planStr += '</ul>';
        } else if (this.bulkUpdate.update_field === 'inventory') {
            reqData.room_types = [];
            reqData.rate_plans = [];
            this.bulkUpdate.selected_room_plans.forEach(rmType => {
                if (rmType.is_selected) {
                    reqData.room_types.push({id: rmType.id, name: rmType.name, inventory: rmType.inventory});
                    planStr += '<li>' + rmType.name + '</li>';
                }
            });
        }

        let updateFields: any;
        updateFields = [];
        if (this.bulkUpdate.update_field === 'rate') {
            updateFields.push('Rates');
        }
        if (this.bulkUpdate.update_field === 'min_nights') {
            updateFields.push('Min Nights');
        }
        if (this.bulkUpdate.update_field === 'max_nights') {
            updateFields.push('Max Nights');
        }
        if (this.bulkUpdate.update_field === 'is_stop_Sell') {
            updateFields.push('Stop Sell');
        }
        if (this.bulkUpdate.update_field === 'coa') {
            updateFields.push('COA');
        }
        if (this.bulkUpdate.update_field === 'cod') {
            updateFields.push('COD');
        }

        if (this.bulkUpdate.update_field === 'inventory') {
            updateFields.push('Inventory');
        }

        let desc: string = '<table class="table table-bordered">' +
            '<tr>' +
            '<td>Hotel: </td><td>' + selHotel.code + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Plans:</td><td> ' + planStr + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Date:</td><td> ' + moment(this.bulkUpdate.fromDate, 'YYYY-MM-DD').format('DD MMM YYYY') + ' - '
            + moment(this.bulkUpdate.toDate, 'YYYY-MM-DD').format('DD MMM YYYY') + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Days:</td><td> ' + reqData.days.join(', ') + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Update:</td><td> ' + updateFields.join(', ') + '</td>' +
            '</tr>';
        desc += '</table>';


        const dialogRef = this.layoutUtilsService.deleteElement(
            'Are you sure to update following ?',
            desc,
            'Updating...',
            'Yes',
            'No'
        );
        dialogRef.afterClosed().subscribe(res => {

            if (!res) {
                return;
            }

            this.isSaving = true;
            this.rateInventoryService.bulkDistributionUpdate(reqData)
                .subscribe(response => {
                        this.isSaving = false;
                        this.toastService.success('Saved Success', 'Success');
                        this.closeDialog(response);
                    },
                    error => {
                        this.isSaving = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                    });
        });
    }

    loadRatePlans() {
        let params: any = {};
        params = {};

        params.hotel_id = this.bulkUpdate.hotel_id;
        // params.room_type_id = this.bulkUpdate.room_type_id;

        this.settingsService.getRatePlans(params)
            .subscribe(response => {
                    this.isLoading = false;
                    this.ratePlans = response.items;

                    this.ratePlans.forEach(ratePln => {
                        if (ratePln.linked_plan_id) {
                            ratePln.isDerived = true;
                        } else {
                            ratePln.isDerived = false;
                        }
                        // ratePln.isDerived = false;
                        // const derivedRatePln = this.ratePlans.find(itm => itm.linked_plan_id === ratePln.id);
                        // if (derivedRatePln) {
                        //     ratePln.isDerived = true;
                        // }
                    });

                    let roomTypesArray;
                    roomTypesArray = [];
                    if (this.roomTypes) {
                        this.roomTypes.forEach(roomType => {
                            const plansByRoomType = this.ratePlans.filter(itm => itm.room_type_id === roomType.id);
                            roomType.plans = plansByRoomType.map(pln => Object.assign({}, pln));

                            roomTypesArray.push(Object.assign({}, roomType));
                        });
                        console.log(this.roomTypes);
                    }

                    this.roomTypes = roomTypesArray;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    canApplyBulkUpdate() {
        let result: boolean;
        result = false;
        let isError: boolean = false;

        if (this.bulkUpdate.selected_room_plans.length > 0) {
            result = true;
        } else {
            isError = true;
        }

        const selDays = this.selectedDays.filter(item => item.is_selected === true);
        if (!isError) {
            if (selDays.length < 1) {
                result = false;
            }
        }

        if (isError) {
            result = false;
        }

        return result;
    }

    ratePlansChange() {
        console.log(this.bulkUpdate.rate_plans);
    }

    roomTypePlansSelectionChange(roomType) {
        roomType.plans.forEach(pln => {
            pln.is_selected = roomType.is_selected;
            pln.mealplans.forEach(mealPln => {
                mealPln.is_selected = pln.is_selected;
            });
        });
        this.setSelectedPlansCount();
    }

    findAndCheckDerivedPlan(pln, roomType) {
        const derivedRatePlns = roomType.plans.filter(itm => pln.id === itm.linked_plan_id);
        if (derivedRatePlns) {
            derivedRatePlns.forEach(derivedRatePln => {
                derivedRatePln.is_selected = pln.is_selected;
                derivedRatePln.mealplans.forEach(mealPln => {
                    mealPln.is_selected = derivedRatePln.is_selected;
                });
                this.findAndCheckDerivedPlan(derivedRatePln, roomType);
            });
        }
    }

    changeSelectionMealPlan() {
        this.setSelectedPlansCount();
    }

    changeSelectionPlan(pln, roomType) {
        this.findAndCheckDerivedPlan(pln, roomType);

        const selectedPlans = roomType.plans.filter(itm => itm.is_selected === true);
        if (selectedPlans.length === roomType.plans.length) {
            roomType.is_selected = true;
        } else {
            roomType.is_selected = false;
        }

        pln.mealplans.forEach(mealPln => {
            mealPln.is_selected = pln.is_selected;
        });
        this.setSelectedPlansCount();
    }

    setSelectedPlansCount() {
        this.selectedPlansCount = 0;
        let selectedPlansArr = [];
        let selectedRoomPlansArr;
        selectedRoomPlansArr = [];
        let selectedMealPlanCount = 0;
        this.roomTypes.forEach(roomType => {
            const selectedPlans = roomType.plans.filter(itm => itm.is_selected === true);
            if (selectedPlans.length > 0) {
                selectedPlansArr = selectedPlansArr.concat(selectedPlans);
                let rmType;
                rmType = Object.assign({}, roomType);
                rmType.plans = selectedPlans;
                selectedRoomPlansArr.push(rmType);

                if (this.bulkUpdate.update_field === 'rate') {
                    rmType.plans.forEach(pln => {
                        pln.mealplans.forEach(mealPlan => {
                            if (mealPlan.is_selected === true) {
                                selectedMealPlanCount++;
                            }
                        });

                    });
                }

            }
            this.selectedPlansCount += selectedPlans.length;
            if (this.bulkUpdate.update_field === 'rate') {
                if (selectedMealPlanCount < 1) {
                    this.selectedPlansCount = 0;
                }
            }

        });
        this.bulkUpdate.rate_plans = selectedPlansArr;
        this.bulkUpdate.selected_room_plans = selectedRoomPlansArr;
    }

    changeTab(index) {
        this.selectedIndex = index;
    }

    changeMasterPlanRate(pln, roomType) {
        const derivedRatePlns = roomType.plans.filter(itm => pln.id === itm.linked_plan_id);
        pln.mealplans.forEach(mealPln => {
            mealPln.rate = pln.rate + mealPln.differential_price;
            mealPln.adult_rate = pln.adult_rate;
            mealPln.child_rate = pln.child_rate;
        });
        if (derivedRatePlns) {
            derivedRatePlns.forEach(derivedRatePln => {
                derivedRatePln.rate = pln.rate + derivedRatePln.lp_rate_diff_price;
                derivedRatePln.adult_rate = pln.adult_rate + derivedRatePln.lp_adult_diff_price;
                derivedRatePln.child_rate = pln.child_rate + derivedRatePln.lp_child_diff_price;
                this.changeMasterPlanRate(derivedRatePln, roomType);
            });
        }
    }

    loadRoomTypes() {
        this.settingsService.getRoomTypeList({hotel_id: this.bulkUpdate.hotel_id})
            .subscribe(response => {
                    this.roomTypes = response;
                    this.loadRatePlans();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getSelectedMealPlans(plan) {
        const mealPlan = plan.mealplans.filter(item => item.is_selected === true);
        return mealPlan;
    }
}
