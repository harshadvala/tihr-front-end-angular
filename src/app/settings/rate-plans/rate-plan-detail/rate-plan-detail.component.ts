import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {TokenStorage} from '../../../core/auth/token-storage.service';
import {UserService} from '../../../services/user.service';
import {environment} from '../../../../environments/environment';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {SettingsService} from '../../../services/settings.service';
import {UtilsService} from '../../../core/services/utils.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {HotelService} from '../../../services/hotel.service';

@Component({
    selector: 'm-rate-plan-detail',
    templateUrl: './rate-plan-detail.component.html',
    styleUrls: ['./rate-plan-detail.component.scss'],
    providers: [UserService, SettingsService, UtilsService, HotelService]
})
export class RatePlanDetailComponent implements OnInit {
    selectedTabIndex: number = 0;
    isSaving: boolean = false;
    isLoading: boolean = false;
    isRequirePsw: boolean = true;
    ratePlans: any[] = [];
    roomTypes: any[] = [];
    hotelRoomTypes: any[] = [];
    bookingTypes: any = [];
    hotels: any[] = [];
    travelAgents: any[] = [];
    ratePlanTypes: any[] = [];
    mealPlans: any[] = [];
    selectedMealPlans: any = [];
    currentRecord: any = {
        max_adult: 0,
        max_child: 0,
        base_adult: 0,
        base_child: 0,
        show_to_all_sources: true,
        conditions: [],
        travel_agents: [],
        highlightedtexts: [],
        mealplans: [],
        is_enabled: true,
    };
    config: any = {};

    genders: any[] = [
        {
            id: 'Male',
            name: 'Male'
        },
        {
            id: 'Female',
            name: 'Female'
        }
    ];
    locationDataSource: DataSource;
    citiesLIst = new FormControl(null, [Validators.required]);
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '18rem',
        minHeight: '5rem',
        placeholder: 'Enter text here...',
        translate: 'no',
        customClasses: [
            {
                name: 'quote',
                class: 'quote',
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: 'titleText',
                class: 'titleText',
                tag: 'h1',
            },
        ]
    };

    constructor(public dialogRef: MatDialogRef<RatePlanDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef,
                private toastService: ToastrService,
                private modalService: NgbModal,
                private settingService: SettingsService,
                private hotelService: HotelService,
                private http: HttpClient,
                private utilsService: UtilsService,
                private tokenStorage: TokenStorage,
                private userService: UserService) {

        this.config = this.utilsService.getCKEditorConfig({toolbarGroups: [{name: 'tools'}]});

        this.isRequirePsw = true;
        if (this.data) {
            console.log(this.data);
            this.ratePlanTypes = data.ratePlanTypes;
            this.roomTypes = data.roomTypes;
            this.hotels = data.hotels;
            this.bookingTypes = data.bookingTypes;
            this.travelAgents = data.travelAgents;
            this.mealPlans = data.mealPlans;
            if (this.data.currentRecord.id) {
                this.getPlanDetail(this.data.currentRecord.id);
            }
            this.getRatePlans();
        }


        this.currentRecord.mealplans.forEach(pln => {
            const plnObj = this.mealPlans.find(itm => itm.id === pln.id);
            if (plnObj) {
                this.selectedMealPlans.push(plnObj);
            }
        });

        const apiURL = environment.API_URL + '/state-cities/12';
        this.locationDataSource = {
            displayValue(value: any): Observable<any | null> {
                console.log('finding display value for', value);
                if (typeof value === 'string') {
                    value = parseInt(value, 10);
                }
                if (typeof value !== 'number') {
                    return of(null);
                }

                return http.get<any>(apiURL + '?id=' + value).pipe(
                    map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))
                );
            },
            search(term: string): Observable<any[]> {
                return http.get<any[]>(apiURL, {
                    params: {
                        pageSize: '10',
                        q: term || '',
                        type: '1',
                        _sort: 'name'
                    }
                }).pipe(
                    map(list => list.map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))));
            }
        };
    }

    ngOnInit() {

    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingService.saveRatePlans(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    dobDateChangeEvent(type: string) {
        this.currentRecord[type] = this.currentRecord[type].format('YYYY-MM-DD HH:mm:ss');
    }

    getHotelRoomTypes() {
        if (!this.currentRecord.hotel_id) {
            return;
        }
        this.hotelService.getHotelRoomTypes(this.currentRecord.hotel_id)
            .subscribe(response => {
                    this.hotelRoomTypes = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getRatePlans() {
        this.getHotelRoomTypes();
        if (!this.currentRecord.hotel_id || !this.currentRecord.room_type_id) {
            return;
        }

        const reqParams = {
            hotel_id: this.currentRecord.hotel_id,
            room_type_id: this.currentRecord.room_type_id,
        };
        this.settingService.getRatePlans(reqParams)
            .subscribe(response => {
                    let items = response.items;
                    if (response.items && this.currentRecord.id) {
                        items = response.items.filter(itm => (itm.id !== this.currentRecord.id
                            && (this.currentRecord.id !== itm.linked_plan_id)));
                    }
                    this.ratePlans = items;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    changeShowCondition() {
        if (this.currentRecord.is_special_conditions) {
            this.currentRecord.conditions = [{
                title: '',
                description: ''
            }];
        } else {
            this.currentRecord.conditions = [];
        }
    }

    addCondition() {
        this.currentRecord.conditions.push({
            title: '',
            description: ''
        });
    }

    removeCondition(index) {
        this.currentRecord.conditions.splice(index, 1);
        if (this.currentRecord.conditions.length === 0) {
            this.currentRecord.is_special_conditions = false;
        }
    }

    changeHTextCondition() {
        if (this.currentRecord.add_highlighted_text) {
            this.currentRecord.highlightedtexts = [{
                caption: ''
            }];
        } else {
            this.currentRecord.highlightedtexts = [];
        }
    }

    addHighlightedText() {
        this.currentRecord.highlightedtexts.push({
            caption: ''
        });
    }

    removeHighlightedText(index) {
        this.currentRecord.highlightedtexts.splice(index, 1);
        if (this.currentRecord.highlightedtexts.length === 0) {
            this.currentRecord.add_highlighted_text = false;
        }
    }

    changeTabTab(tabId) {
        this.selectedTabIndex = tabId;
    }


    changeMealPlanSelection() {
        this.currentRecord.mealplans.forEach(pln => {
            const plnObj = this.selectedMealPlans.find(itm => itm.id === pln.id);
            if (!plnObj) {
                this.currentRecord.mealplans.splice(this.currentRecord.mealplans.indexOf(pln), 1);
            }
        });

        this.selectedMealPlans.forEach(pln => {
            const plnObj = this.currentRecord.mealplans.find(itm => itm.id === pln.id);
            if (!plnObj) {
                let planDetail = Object.assign({}, pln);
                planDetail.differential_price = pln.price;
                this.currentRecord.mealplans.push(planDetail);
            }
        });
    }

    linkPlan() {
        if (!this.currentRecord.linked_plan_id) {
            this.currentRecord.lp_rate_diff_price = 0;
        }
    }

    getPlanDetail(id) {
        this.isLoading = true;
        this.settingService.findRatePlan(id)
            .subscribe(response => {
                    this.currentRecord = response;
                    this.isLoading = false;
                    this.isRequirePsw = false;
                    this.citiesLIst.setValue(this.currentRecord.location_id);
                    this.getRatePlans();
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }
}
