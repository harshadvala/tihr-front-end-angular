import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../services/user.service';
import {MatDialog, MatPaginator, MatSlideToggleChange, MatSort} from '@angular/material';
import {fromEvent, merge} from 'rxjs';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {SelectionModel} from '@angular/cdk/collections';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {UserDataSource} from '../../Models/data-sources/user.dataSource';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {CommonService} from '../../services/common.service';
import {ToastrService} from 'ngx-toastr';
import {HotelService} from '../../services/hotel.service';
import {RoleService} from '../../services/role.service';
import {UtilsService} from '../../core/services/utils.service';

@Component({
    selector: 'm-users-management',
    templateUrl: './users-management.component.html',
    styleUrls: ['./users-management.component.scss'],
    providers: [CommonService, UserService, HotelService, RoleService, UtilsService]
})
export class UsersManagementComponent implements OnInit {
    dataSource: any = UserDataSource;
    users: any[] = [];
    designations: any[] = [];
    departments: any[] = [];
    userRoles: any[] = [];
    hotels: any[] = [];
    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    isCollapsedFilter: boolean = true;
    customFilters: any = {
        status: '',
        ota_booking_assign: '',
    };
    hotelList: any = [];
    hotelListSettings: any = {};
    selectedHotelLists = [];
    designationList: any = [];
    roleList: any = [];
    designationListSettings: any = {};
    roleListSettings: any = {};
    selectedDesignationLists = [];
    selectedRoleLists = [];

    /*citiesList: any = [];
    citiesSettings: any = {};
    selectedCitiesLists = [];*/

    constructor(public dialog: MatDialog,
                private layoutUtilsService: LayoutUtilsService,
                private utilsService: UtilsService,
                private commonService: CommonService,
                private toastService: ToastrService,
                private roleService: RoleService,
                private hotelService: HotelService,
                public userService: UserService) {

        this.hotelListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Hotels', labelKey: 'code'});
        this.designationListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Designation', badgeShowLimit: 1});
        this.roleListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Role', badgeShowLimit: 1, labelKey: 'display_name'});
        /*this.citiesSettings = this.utilsService.getMultiSelctConfig({text: 'Select Office Location', badgeShowLimit: 1});*/
    }

    displayedColumns: string[] = [
        'select',
        'name',
        'user_name',
        'mobile',
        'email',
        'location',
        'role_name',
        'status',
        'actions'
    ];

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadUserList())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadUserList();
                })
            )
            .subscribe();


        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadUserList();
                })
            )
            .subscribe();

        let queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );

        queryParams = Object.assign(queryParams, this.customFilters);

        this.dataSource = new UserDataSource(this.userService);
        this.dataSource.loadUsers(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.users = res;
        });


        //
        this.getUserDesignations();
        this.getDepartments();
        this.getHotels();
        this.getRoles();
    }

    onStatusChange(currentRecord, event: MatSlideToggleChange) {
        const dialogRef = this.layoutUtilsService.deleteElement(
            'Confirm Operation',
            'Are you sure to perform action?',
            'saving...',
            'Yes',
            'No'
        );
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                currentRecord.status = !event.checked;
                return;
            }
            this.save(currentRecord, event.checked);
        });
    }

    save(currentRecord, newStatus) {
        // currentRecord.name = currentRecord.first_name + ' ' + currentRecord.last_name;
        currentRecord.isSaving = true;
        currentRecord.status = newStatus;
        currentRecord.permissions = [];
        this.userService.save(currentRecord)
            .subscribe(response => {
                    currentRecord.isSaving = false;
                    currentRecord.status = response.status;
                    this.toastService.success('Saved Successfully', 'Success');
                },
                error => {
                    currentRecord.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    loadUserList() {
        const q = this.searchInput.nativeElement.value;
        let queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);

        queryParams = Object.assign(queryParams, this.customFilters);

        this.dataSource.loadUsers(queryParams);
        this.selection.clear();
    }

    openUserDetail(detail: any = {}) {
        const dialogRef = this.dialog.open(UserDetailComponent, {
            data: {
                currentRecord: (detail) ? Object.assign({}, detail) : {},
                designations: this.designations,
                departments: this.departments,
                hotels: this.hotels,
                userRoles: this.userRoles
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadUserList();
            }
        });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.users.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.users.length) {
            this.selection.clear();
        } else {
            this.users.forEach(row => this.selection.select(row));
        }
    }

    deleteUser(user) {
        //
    }

    getUserDesignations() {
        this.commonService.getDesignation()
            .subscribe(response => {
                    this.designations = response;
                    this.designationList = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getDepartments() {
        this.commonService.getDepartments()
            .subscribe(response => {
                    this.departments = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getRoles() {
        this.roleService.getRoles()
            .subscribe(response => {
                    this.userRoles = response;
                    this.roleList = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getHotels() {
        this.hotelService.getHotelList()
            .subscribe(response => {
                    this.hotels = response;
                    this.hotelList = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    initFilters() {
        this.customFilters = {
            status: '',
            ota_booking_assign: '',
        };
    }

    applyFilter() {
        this.paginator.pageIndex = 0;
        this.loadUserList();
    }

    resetFilter() {
        this.selectedHotelLists = [];
        this.selectedDesignationLists = [];
        this.selectedRoleLists = [];
        /*this.selectedCitiesLists = [];*/
        this.isCollapsedFilter = true;
        this.initFilters();
        this.paginator.pageIndex = 0;
        this.loadUserList();
    }

    onHotelSelect(item: any = {}) {
        this.customFilters.hotel_id = [];
        if (this.selectedHotelLists.length > 0) {
            this.selectedHotelLists.forEach(itm => {
                this.customFilters.hotel_id.push(itm.id);
            });
            this.customFilters.hotel_id = this.customFilters.hotel_id.join();
        }
    }

    onDesignationSelect(item: any = {}) {
        this.customFilters.designation_id = [];
        if (this.selectedDesignationLists.length > 0) {
            this.selectedDesignationLists.forEach(itm => {
                this.customFilters.designation_id.push(itm.id);
            });
            this.customFilters.designation_id = this.customFilters.designation_id.join();
        }
    }

    onRoleSelect(item: any = {}) {
        this.customFilters.role_id = [];
        if (this.selectedRoleLists.length > 0) {
            this.selectedRoleLists.forEach(itm => {
                this.customFilters.role_id.push(itm.id);
            });
            this.customFilters.role_id = this.customFilters.role_id.join();
        }
    }

    /*onCitiesSelect(item: any = {}) {
        this.customFilters.location_id = [];
        if (this.selectedCitiesLists.length > 0) {
            this.selectedCitiesLists.forEach(itm => {
                this.customFilters.location_id.push(itm.id);
            });
            this.customFilters.location_id = this.customFilters.location_id.join();
        }
    }*/

}
