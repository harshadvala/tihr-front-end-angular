import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {

    transform(value: any[], searchTerm: string): any {
        if (!searchTerm) {
            return value;
        }
        const result = [];
        if (Array.isArray(value)) {
            value.reduce(function (prevItem: any, currentItem: any): void {
                if (currentItem instanceof Array || currentItem instanceof Object) {

                    for (const key in currentItem) {
                        if (!currentItem.hasOwnProperty(key)) {
                            continue;
                        }
                        const term = currentItem[key];

                        if (term instanceof Array || term === null || term instanceof Object) {
                            continue;
                        }

                        if (term.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                            result.push(currentItem);
                            break;
                        }
                    }
                } else {
                    if (currentItem.toString().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
                        result.push(currentItem);
                    }
                }
            }, 0);
            return result;
        }
    }
}
