import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'sum'
})
export class SumPipe implements PipeTransform {
    transform(items: any, attr: string): any {
        let sum = 0;
        for (let i = 0; i < items.length; i++) {
            sum += parseFloat(items[i][attr]);
        }
        return sum;
    }
}
